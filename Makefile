all:
	yarn run build

clean:
	yarn run clean

pull-protocols:
	git subtree pull -P protocols \
		-m 'Merge latest changes from the syndicate-protocols repository' \
		git@git.syndicate-lang.org:syndicate-lang/syndicate-protocols \
		main
