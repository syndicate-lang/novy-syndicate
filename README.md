# novy-syndicate

A follow-on from Syndicate-2017, incorporating object-capability-based
securability and other ideas from E.

You can view this as Syndicate plus locations and capabilities, or as
E plus Syndicate-style shared state and fault-tolerance.

The capabilities offer *securability* of Syndicate-style
point-to-point and multiparty communications, a necessary precondition
for wider use of Syndicate-like ideas.

The locations offer abstraction over *distribution* of Syndicate
systems, a necessary precondition for modular reasoning about and
reuse of Syndicate subsystems.

## Take it for a spin

    git clone https://git.syndicate-lang.org/syndicate-lang/novy-syndicate
    cd novy-syndicate
    yarn install
    yarn build

Start a server containing a single dataspace:

    node -r esm lib/distributed/server.js

It will print out a "root" capability giving full control of the
contents of the dataspace:

    $ node -r esm lib/distributed/server.js
    <ref "syndicate" [] #"\xa6H\x0d\xf50f\x11\xdd\xd0\xd3\x88+Tn\x19w">
    b4b303726566b10973796e646963617465b584b210a6480df5306611ddd0d3882b546e197784

Next, try running the `simple-chat` example in a separate terminal:

    node -r esm lib/distributed/sandbox.js ../examples/simple-chat.js \
        b4b303726566b10973796e646963617465b584b210a6480df5306611ddd0d3882b546e197784

Note that the second command-line argument is the capability to use to
gain access to the server.

Run the same command again in another separate terminal. Typing into
one terminal will be relayed to the other; the command `/nick
<newnick>` will change nickname.

Next, generate an *attenuated* capability which will only allow
interaction via the nickname `tonyg`:

    node -r esm lib/tools/attenuate.js \
        b4b303726566b10973796e646963617465b584b210a6480df5306611ddd0d3882b546e197784 \
        '[<or [
           <rewrite <bind p <compound <rec Present 1> {0: <lit "tonyg">}>> <ref p>>
           <rewrite <bind p <compound <rec Says 2> {0: <lit "tonyg"> 1: String}>> <ref p>>
         ]>]'

The result is the following data structure:

    <ref "syndicate" [[<or [
      <rewrite <bind p <compound <rec Present 1> {0: <lit "tonyg">}>> <ref p>>,
      <rewrite <bind p <compound <rec Says 2> {
        0: <lit "tonyg">,
        1: String
      }>> <ref p>>
    ]>]] #[oHFy7B4NPVqhD6zJmNPbhg==]>

It is a
[Macaroon](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/41892.pdf),
based on the "root" capability, but with additional restrictions added
to it.

To try it out, terminate one of the chat clients, and restart it using
the hex form of the attenuated capability:

    node -r esm lib/distributed/sandbox.js ../examples/simple-chat.js \
        b4b303726566b10973796e646963617465b5b5b4b3026f72b5b4b30772657772697465b4b30462696e64b30170b4b308636f6d706f756e64b4b303726563b30750726573656e749184b790b4b3036c6974b105746f6e796784848484b4b303726566b301708484b4b30772657772697465b4b30462696e64b30170b4b308636f6d706f756e64b4b303726563b304536179739284b790b4b3036c6974b105746f6e79678491b306537472696e67848484b4b303726566b30170848484848484b210a07172ec1e0d3d5aa10facc998d3db8684

Notice that (a) this new client can't hear anything from its peers and
(b) can't send anything either -- *until* it changes its nickname to
`tonyg` (via `/nick tonyg`). Then, its peers can hear it. But to allow
it to hear its peers, we need to add another option when we attenuate
the root capability:

    node -r esm lib/tools/attenuate.js \
        b4b303726566b10973796e646963617465b584b210a6480df5306611ddd0d3882b546e197784 \
        '[<or [
           <rewrite <bind p <compound <rec Present 1> {0: <lit "tonyg">}>> <ref p>>
           <rewrite <bind p <compound <rec Says 2> {0: <lit "tonyg"> 1: String}>> <ref p>>
           <rewrite <bind p <compound <rec Observe 2> {}>> <ref p>>
         ]>]'

The result,

    <ref "syndicate" [[<or [
      <rewrite <bind p <compound <rec Present 1> {0: <lit "tonyg">}>> <ref p>>,
      <rewrite <bind p <compound <rec Says 2> {
        0: <lit "tonyg">,
        1: String
      }>> <ref p>>,
      <rewrite <bind p <compound <rec Observe 2> {}>> <ref p>>
    ]>]] #[FqMH2fgbrM29dQedmuFclg==]>

allows assertion of `<Present "tonyg">`, transmission of `<Says
"tonyg" _>`, and observation of anything at all in the dataspace, with
assertion of `<Observe _ _>`.

Rerunning the chat client with the hex form of the capability shows
off the new behaviour:

    node -r esm lib/distributed/sandbox.js ../examples/simple-chat.js \
        b4b303726566b10973796e646963617465b5b5b4b3026f72b5b4b30772657772697465b4b30462696e64b30170b4b308636f6d706f756e64b4b303726563b30750726573656e749184b790b4b3036c6974b105746f6e796784848484b4b303726566b301708484b4b30772657772697465b4b30462696e64b30170b4b308636f6d706f756e64b4b303726563b304536179739284b790b4b3036c6974b105746f6e79678491b306537472696e67848484b4b303726566b301708484b4b30772657772697465b4b30462696e64b30170b4b308636f6d706f756e64b4b303726563b3074f6273657276659284b7848484b4b303726566b30170848484848484b21016a307d9f81baccdbd75079d9ae15c9684
