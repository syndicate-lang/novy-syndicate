import { Bytes, Reader } from '@preserves/core';
import { fromSturdyRef, toSturdyRef, toCaveat, attenuate, sturdyDecode, sturdyEncode, _embedded } from '../transport/sturdy.js';

const [ base, pat ] = process.argv.slice(2);
const baseCap = toSturdyRef(sturdyDecode(Bytes.fromHex(base ?? '')));
if (baseCap === void 0) throw new Error("Cannot decode sturdyref");
const cs0 = new Reader<_embedded>(pat).next();
if (!Array.isArray(cs0)) throw new Error("Expected array of caveats");
const cs = cs0.map(c => toCaveat(c) ?? (()=>{ throw new Error("Cannot decode caveat"); })());
attenuate(baseCap, ... cs).then(derived => {
    console.log(fromSturdyRef(derived).asPreservesText());
    console.log(sturdyEncode(fromSturdyRef(derived)).toHex());
});
