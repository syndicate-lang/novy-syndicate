const LIMIT = 25000;

let taskCounter = 0;
let delayedTasks: Array<() => void> = [];
export function queueTask(f: () => void) {
    taskCounter++;
    if (taskCounter === LIMIT) {
        setTimeout(() => {
            taskCounter = 0;
            delayedTasks.forEach(queueMicrotask);
            delayedTasks = [];
        }, 0);
    }
    if (taskCounter >= LIMIT) {
        delayedTasks.push(f);
    } else {
        queueMicrotask(f);
    }
}
