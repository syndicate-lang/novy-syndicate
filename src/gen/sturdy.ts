import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $0 = 0;
export const $1 = 1;
export const $Boolean = Symbol.for("Boolean");
export const $ByteString = Symbol.for("ByteString");
export const $Double = Symbol.for("Double");
export const $Embedded = Symbol.for("Embedded");
export const $Float = Symbol.for("Float");
export const $SignedInteger = Symbol.for("SignedInteger");
export const $String = Symbol.for("String");
export const $Symbol = Symbol.for("Symbol");
export const $_ = Symbol.for("_");
export const $and = Symbol.for("and");
export const $arr = Symbol.for("arr");
export const $attenuate = Symbol.for("attenuate");
export const $bind = Symbol.for("bind");
export const $compound = Symbol.for("compound");
export const $dict = Symbol.for("dict");
export const $lit = Symbol.for("lit");
export const $not = Symbol.for("not");
export const $or = Symbol.for("or");
export const $rec = Symbol.for("rec");
export const $ref = Symbol.for("ref");
export const $rewrite = Symbol.for("rewrite");

export type _embedded = _i_EntityRef.Cap;

export type SturdyRef<_embedded = _i_EntityRef.Cap> = {
    "oid": _.Value<_embedded>,
    "caveatChain": Array<Attenuation<_embedded>>,
    "sig": _.Bytes
};

export type Attenuation<_embedded = _i_EntityRef.Cap> = Array<Caveat<_embedded>>;

export type Caveat<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "Rewrite", "value": Rewrite<_embedded>} |
    {"_variant": "Alts", "value": Alts<_embedded>}
);

export type Rewrite<_embedded = _i_EntityRef.Cap> = {"pattern": Pattern<_embedded>, "template": Template<_embedded>};

export type Alts<_embedded = _i_EntityRef.Cap> = {"alternatives": Array<Rewrite<_embedded>>};

export type Oid = number;

export type WireRef<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "mine", "oid": Oid} |
    {"_variant": "yours", "oid": Oid, "attenuation": Array<Caveat<_embedded>>}
);

export type ConstructorSpec<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "CRec", "value": CRec<_embedded>} |
    {"_variant": "CArr", "value": CArr} |
    {"_variant": "CDict", "value": CDict}
);

export type CRec<_embedded = _i_EntityRef.Cap> = {"label": _.Value<_embedded>, "arity": number};

export type CArr = {"arity": number};

export type CDict = null;

export type Lit<_embedded = _i_EntityRef.Cap> = {"value": _.Value<_embedded>};

export type Pattern<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "PDiscard", "value": PDiscard} |
    {"_variant": "PAtom", "value": PAtom} |
    {"_variant": "PEmbedded", "value": PEmbedded} |
    {"_variant": "PBind", "value": PBind<_embedded>} |
    {"_variant": "PAnd", "value": PAnd<_embedded>} |
    {"_variant": "PNot", "value": PNot<_embedded>} |
    {"_variant": "Lit", "value": Lit<_embedded>} |
    {"_variant": "PCompound", "value": PCompound<_embedded>}
);

export type PDiscard = null;

export type PAtom = (
    {"_variant": "Boolean"} |
    {"_variant": "Float"} |
    {"_variant": "Double"} |
    {"_variant": "SignedInteger"} |
    {"_variant": "String"} |
    {"_variant": "ByteString"} |
    {"_variant": "Symbol"}
);

export type PEmbedded = null;

export type PBind<_embedded = _i_EntityRef.Cap> = {"pattern": Pattern<_embedded>};

export type PAnd<_embedded = _i_EntityRef.Cap> = {"patterns": Array<Pattern<_embedded>>};

export type PNot<_embedded = _i_EntityRef.Cap> = {"pattern": Pattern<_embedded>};

export type PCompound<_embedded = _i_EntityRef.Cap> = {"ctor": ConstructorSpec<_embedded>, "members": PCompoundMembers<_embedded>};

export type PCompoundMembers<_embedded = _i_EntityRef.Cap> = _.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>;

export type Template<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "TAttenuate", "value": TAttenuate<_embedded>} |
    {"_variant": "TRef", "value": TRef} |
    {"_variant": "Lit", "value": Lit<_embedded>} |
    {"_variant": "TCompound", "value": TCompound<_embedded>}
);

export type TAttenuate<_embedded = _i_EntityRef.Cap> = {"template": Template<_embedded>, "attenuation": Attenuation<_embedded>};

export type TRef = {"binding": number};

export type TCompound<_embedded = _i_EntityRef.Cap> = {"ctor": ConstructorSpec<_embedded>, "members": TCompoundMembers<_embedded>};

export type TCompoundMembers<_embedded = _i_EntityRef.Cap> = _.KeyedDictionary<_.Value<_embedded>, Template<_embedded>, _embedded>;


export function SturdyRef<_embedded = _i_EntityRef.Cap>(
    {oid, caveatChain, sig}: {
        oid: _.Value<_embedded>,
        caveatChain: Array<Attenuation<_embedded>>,
        sig: _.Bytes
    }
): SturdyRef<_embedded> {return {"oid": oid, "caveatChain": caveatChain, "sig": sig};}

export function Attenuation<_embedded = _i_EntityRef.Cap>(value: Array<Caveat<_embedded>>): Attenuation<_embedded> {return value;}

export namespace Caveat {
    export function Rewrite<_embedded = _i_EntityRef.Cap>(value: Rewrite<_embedded>): Caveat<_embedded> {return {"_variant": "Rewrite", "value": value};};
    export function Alts<_embedded = _i_EntityRef.Cap>(value: Alts<_embedded>): Caveat<_embedded> {return {"_variant": "Alts", "value": value};};
}

export function Rewrite<_embedded = _i_EntityRef.Cap>(
    {pattern, template}: {pattern: Pattern<_embedded>, template: Template<_embedded>}
): Rewrite<_embedded> {return {"pattern": pattern, "template": template};}

export function Alts<_embedded = _i_EntityRef.Cap>(alternatives: Array<Rewrite<_embedded>>): Alts<_embedded> {return {"alternatives": alternatives};}

export function Oid(value: number): Oid {return value;}

export namespace WireRef {
    export function mine<_embedded = _i_EntityRef.Cap>(oid: Oid): WireRef<_embedded> {return {"_variant": "mine", "oid": oid};};
    export function yours<_embedded = _i_EntityRef.Cap>({oid, attenuation}: {oid: Oid, attenuation: Array<Caveat<_embedded>>}): WireRef<_embedded> {return {"_variant": "yours", "oid": oid, "attenuation": attenuation};};
}

export namespace ConstructorSpec {
    export function CRec<_embedded = _i_EntityRef.Cap>(value: CRec<_embedded>): ConstructorSpec<_embedded> {return {"_variant": "CRec", "value": value};};
    export function CArr<_embedded = _i_EntityRef.Cap>(value: CArr): ConstructorSpec<_embedded> {return {"_variant": "CArr", "value": value};};
    export function CDict<_embedded = _i_EntityRef.Cap>(value: CDict): ConstructorSpec<_embedded> {return {"_variant": "CDict", "value": value};};
}

export function CRec<_embedded = _i_EntityRef.Cap>({label, arity}: {label: _.Value<_embedded>, arity: number}): CRec<_embedded> {return {"label": label, "arity": arity};}

export function CArr(arity: number): CArr {return {"arity": arity};}

export function CDict(): CDict {return null;}

export function Lit<_embedded = _i_EntityRef.Cap>(value: _.Value<_embedded>): Lit<_embedded> {return {"value": value};}

export namespace Pattern {
    export function PDiscard<_embedded = _i_EntityRef.Cap>(value: PDiscard): Pattern<_embedded> {return {"_variant": "PDiscard", "value": value};};
    export function PAtom<_embedded = _i_EntityRef.Cap>(value: PAtom): Pattern<_embedded> {return {"_variant": "PAtom", "value": value};};
    export function PEmbedded<_embedded = _i_EntityRef.Cap>(value: PEmbedded): Pattern<_embedded> {return {"_variant": "PEmbedded", "value": value};};
    export function PBind<_embedded = _i_EntityRef.Cap>(value: PBind<_embedded>): Pattern<_embedded> {return {"_variant": "PBind", "value": value};};
    export function PAnd<_embedded = _i_EntityRef.Cap>(value: PAnd<_embedded>): Pattern<_embedded> {return {"_variant": "PAnd", "value": value};};
    export function PNot<_embedded = _i_EntityRef.Cap>(value: PNot<_embedded>): Pattern<_embedded> {return {"_variant": "PNot", "value": value};};
    export function Lit<_embedded = _i_EntityRef.Cap>(value: Lit<_embedded>): Pattern<_embedded> {return {"_variant": "Lit", "value": value};};
    export function PCompound<_embedded = _i_EntityRef.Cap>(value: PCompound<_embedded>): Pattern<_embedded> {return {"_variant": "PCompound", "value": value};};
}

export function PDiscard(): PDiscard {return null;}

export namespace PAtom {
    export function Boolean(): PAtom {return {"_variant": "Boolean"};};
    export function Float(): PAtom {return {"_variant": "Float"};};
    export function Double(): PAtom {return {"_variant": "Double"};};
    export function SignedInteger(): PAtom {return {"_variant": "SignedInteger"};};
    export function String(): PAtom {return {"_variant": "String"};};
    export function ByteString(): PAtom {return {"_variant": "ByteString"};};
    export function Symbol(): PAtom {return {"_variant": "Symbol"};};
}

export function PEmbedded(): PEmbedded {return null;}

export function PBind<_embedded = _i_EntityRef.Cap>(pattern: Pattern<_embedded>): PBind<_embedded> {return {"pattern": pattern};}

export function PAnd<_embedded = _i_EntityRef.Cap>(patterns: Array<Pattern<_embedded>>): PAnd<_embedded> {return {"patterns": patterns};}

export function PNot<_embedded = _i_EntityRef.Cap>(pattern: Pattern<_embedded>): PNot<_embedded> {return {"pattern": pattern};}

export function PCompound<_embedded = _i_EntityRef.Cap>(
    {ctor, members}: {ctor: ConstructorSpec<_embedded>, members: PCompoundMembers<_embedded>}
): PCompound<_embedded> {return {"ctor": ctor, "members": members};}

export function PCompoundMembers<_embedded = _i_EntityRef.Cap>(value: _.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>): PCompoundMembers<_embedded> {return value;}

export namespace Template {
    export function TAttenuate<_embedded = _i_EntityRef.Cap>(value: TAttenuate<_embedded>): Template<_embedded> {return {"_variant": "TAttenuate", "value": value};};
    export function TRef<_embedded = _i_EntityRef.Cap>(value: TRef): Template<_embedded> {return {"_variant": "TRef", "value": value};};
    export function Lit<_embedded = _i_EntityRef.Cap>(value: Lit<_embedded>): Template<_embedded> {return {"_variant": "Lit", "value": value};};
    export function TCompound<_embedded = _i_EntityRef.Cap>(value: TCompound<_embedded>): Template<_embedded> {return {"_variant": "TCompound", "value": value};};
}

export function TAttenuate<_embedded = _i_EntityRef.Cap>(
    {template, attenuation}: {template: Template<_embedded>, attenuation: Attenuation<_embedded>}
): TAttenuate<_embedded> {return {"template": template, "attenuation": attenuation};}

export function TRef(binding: number): TRef {return {"binding": binding};}

export function TCompound<_embedded = _i_EntityRef.Cap>(
    {ctor, members}: {ctor: ConstructorSpec<_embedded>, members: TCompoundMembers<_embedded>}
): TCompound<_embedded> {return {"ctor": ctor, "members": members};}

export function TCompoundMembers<_embedded = _i_EntityRef.Cap>(value: _.KeyedDictionary<_.Value<_embedded>, Template<_embedded>, _embedded>): TCompoundMembers<_embedded> {return value;}

export function asSturdyRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): SturdyRef<_embedded> {
    let result = toSturdyRef(v);
    if (result === void 0) throw new TypeError(`Invalid SturdyRef: ${_.stringify(v)}`);
    return result;
}

export function toSturdyRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | SturdyRef<_embedded> {
    let result: undefined | SturdyRef<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $ref) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (Array<Attenuation<_embedded>>) | undefined;
                _tmp2 = void 0;
                if (_.Array.isArray(v[1])) {
                    _tmp2 = [];
                    for (const _tmp3 of v[1]) {
                        let _tmp4: (Attenuation<_embedded>) | undefined;
                        _tmp4 = toAttenuation(_tmp3);
                        if (_tmp4 !== void 0) {_tmp2.push(_tmp4); continue;};
                        _tmp2 = void 0;
                        break;
                    };
                };
                if (_tmp2 !== void 0) {
                    let _tmp5: (_.Bytes) | undefined;
                    _tmp5 = _.Bytes.isBytes(v[2]) ? v[2] : void 0;
                    if (_tmp5 !== void 0) {result = {"oid": _tmp1, "caveatChain": _tmp2, "sig": _tmp5};};
                };
            };
        };
    };
    return result;
}

export function fromSturdyRef<_embedded = _i_EntityRef.Cap>(_v: SturdyRef<_embedded>): _.Value<_embedded> {
    return _.Record(
        $ref,
        [
            _v["oid"],
            _v["caveatChain"].map(v => fromAttenuation<_embedded>(v)),
            _v["sig"]
        ]
    );
}

export function asAttenuation<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Attenuation<_embedded> {
    let result = toAttenuation(v);
    if (result === void 0) throw new TypeError(`Invalid Attenuation: ${_.stringify(v)}`);
    return result;
}

export function toAttenuation<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Attenuation<_embedded> {
    let _tmp0: (Array<Caveat<_embedded>>) | undefined;
    let result: undefined | Attenuation<_embedded>;
    _tmp0 = void 0;
    if (_.Array.isArray(v)) {
        _tmp0 = [];
        for (const _tmp1 of v) {
            let _tmp2: (Caveat<_embedded>) | undefined;
            _tmp2 = toCaveat(_tmp1);
            if (_tmp2 !== void 0) {_tmp0.push(_tmp2); continue;};
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromAttenuation<_embedded = _i_EntityRef.Cap>(_v: Attenuation<_embedded>): _.Value<_embedded> {return _v.map(v => fromCaveat<_embedded>(v));}

export function asCaveat<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Caveat<_embedded> {
    let result = toCaveat(v);
    if (result === void 0) throw new TypeError(`Invalid Caveat: ${_.stringify(v)}`);
    return result;
}

export function toCaveat<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Caveat<_embedded> {
    let _tmp0: (Rewrite<_embedded>) | undefined;
    let result: undefined | Caveat<_embedded>;
    _tmp0 = toRewrite(v);
    if (_tmp0 !== void 0) {result = {"_variant": "Rewrite", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (Alts<_embedded>) | undefined;
        _tmp1 = toAlts(v);
        if (_tmp1 !== void 0) {result = {"_variant": "Alts", "value": _tmp1};};
    };
    return result;
}

export function fromCaveat<_embedded = _i_EntityRef.Cap>(_v: Caveat<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "Rewrite": {return fromRewrite<_embedded>(_v.value);};
        case "Alts": {return fromAlts<_embedded>(_v.value);};
    };
}

export function asRewrite<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Rewrite<_embedded> {
    let result = toRewrite(v);
    if (result === void 0) throw new TypeError(`Invalid Rewrite: ${_.stringify(v)}`);
    return result;
}

export function toRewrite<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Rewrite<_embedded> {
    let result: undefined | Rewrite<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $rewrite) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (Template<_embedded>) | undefined;
                _tmp2 = toTemplate(v[1]);
                if (_tmp2 !== void 0) {result = {"pattern": _tmp1, "template": _tmp2};};
            };
        };
    };
    return result;
}

export function fromRewrite<_embedded = _i_EntityRef.Cap>(_v: Rewrite<_embedded>): _.Value<_embedded> {
    return _.Record(
        $rewrite,
        [fromPattern<_embedded>(_v["pattern"]), fromTemplate<_embedded>(_v["template"])]
    );
}

export function asAlts<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Alts<_embedded> {
    let result = toAlts(v);
    if (result === void 0) throw new TypeError(`Invalid Alts: ${_.stringify(v)}`);
    return result;
}

export function toAlts<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Alts<_embedded> {
    let result: undefined | Alts<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $or) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Array<Rewrite<_embedded>>) | undefined;
            _tmp1 = void 0;
            if (_.Array.isArray(v[0])) {
                _tmp1 = [];
                for (const _tmp2 of v[0]) {
                    let _tmp3: (Rewrite<_embedded>) | undefined;
                    _tmp3 = toRewrite(_tmp2);
                    if (_tmp3 !== void 0) {_tmp1.push(_tmp3); continue;};
                    _tmp1 = void 0;
                    break;
                };
            };
            if (_tmp1 !== void 0) {result = {"alternatives": _tmp1};};
        };
    };
    return result;
}

export function fromAlts<_embedded = _i_EntityRef.Cap>(_v: Alts<_embedded>): _.Value<_embedded> {
    return _.Record($or, [_v["alternatives"].map(v => fromRewrite<_embedded>(v))]);
}

export function asOid<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Oid {
    let result = toOid(v);
    if (result === void 0) throw new TypeError(`Invalid Oid: ${_.stringify(v)}`);
    return result;
}

export function toOid<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Oid {
    let _tmp0: (number) | undefined;
    let result: undefined | Oid;
    _tmp0 = typeof v === 'number' ? v : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromOid<_embedded = _i_EntityRef.Cap>(_v: Oid): _.Value<_embedded> {return _v;}

export function asWireRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): WireRef<_embedded> {
    let result = toWireRef(v);
    if (result === void 0) throw new TypeError(`Invalid WireRef: ${_.stringify(v)}`);
    return result;
}

export function toWireRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | WireRef<_embedded> {
    let result: undefined | WireRef<_embedded>;
    if (_.Array.isArray(v) && v.length === 2) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v[0], $0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Oid) | undefined;
            _tmp1 = toOid(v[1]);
            if (_tmp1 !== void 0) {result = {"_variant": "mine", "oid": _tmp1};};
        };
    };
    if (result === void 0) {
        if (_.Array.isArray(v) && v.length >= 2) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v[0], $1) ? null : void 0;
            if (_tmp2 !== void 0) {
                let _tmp3: (Oid) | undefined;
                _tmp3 = toOid(v[1]);
                if (_tmp3 !== void 0) {
                    let _tmp4: (Array<_.Value<_embedded>>) | undefined;
                    let _tmp5: (Array<Caveat<_embedded>>) | undefined;
                    _tmp4 = v.slice(2);
                    _tmp5 = [];
                    for (const _tmp6 of _tmp4) {
                        let _tmp7: (Caveat<_embedded>) | undefined;
                        _tmp7 = toCaveat(_tmp6);
                        if (_tmp7 !== void 0) {_tmp5.push(_tmp7); continue;};
                        _tmp5 = void 0;
                        break;
                    };
                    if (_tmp5 !== void 0) {result = {"_variant": "yours", "oid": _tmp3, "attenuation": _tmp5};};
                };
            };
        };
    };
    return result;
}

export function fromWireRef<_embedded = _i_EntityRef.Cap>(_v: WireRef<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "mine": {return [$0, fromOid<_embedded>(_v["oid"])];};
        case "yours": {
            return [
                $1,
                fromOid<_embedded>(_v["oid"]),
                ... _v["attenuation"].map(v => fromCaveat<_embedded>(v))
            ];
        };
    };
}

export function asConstructorSpec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): ConstructorSpec<_embedded> {
    let result = toConstructorSpec(v);
    if (result === void 0) throw new TypeError(`Invalid ConstructorSpec: ${_.stringify(v)}`);
    return result;
}

export function toConstructorSpec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | ConstructorSpec<_embedded> {
    let _tmp0: (CRec<_embedded>) | undefined;
    let result: undefined | ConstructorSpec<_embedded>;
    _tmp0 = toCRec(v);
    if (_tmp0 !== void 0) {result = {"_variant": "CRec", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (CArr) | undefined;
        _tmp1 = toCArr(v);
        if (_tmp1 !== void 0) {result = {"_variant": "CArr", "value": _tmp1};};
        if (result === void 0) {
            let _tmp2: (CDict) | undefined;
            _tmp2 = toCDict(v);
            if (_tmp2 !== void 0) {result = {"_variant": "CDict", "value": _tmp2};};
        };
    };
    return result;
}

export function fromConstructorSpec<_embedded = _i_EntityRef.Cap>(_v: ConstructorSpec<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "CRec": {return fromCRec<_embedded>(_v.value);};
        case "CArr": {return fromCArr<_embedded>(_v.value);};
        case "CDict": {return fromCDict<_embedded>(_v.value);};
    };
}

export function asCRec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CRec<_embedded> {
    let result = toCRec(v);
    if (result === void 0) throw new TypeError(`Invalid CRec: ${_.stringify(v)}`);
    return result;
}

export function toCRec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CRec<_embedded> {
    let result: undefined | CRec<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $rec) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"label": _tmp1, "arity": _tmp2};};
            };
        };
    };
    return result;
}

export function fromCRec<_embedded = _i_EntityRef.Cap>(_v: CRec<_embedded>): _.Value<_embedded> {return _.Record($rec, [_v["label"], _v["arity"]]);}

export function asCArr<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CArr {
    let result = toCArr(v);
    if (result === void 0) throw new TypeError(`Invalid CArr: ${_.stringify(v)}`);
    return result;
}

export function toCArr<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CArr {
    let result: undefined | CArr;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $arr) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"arity": _tmp1};};
        };
    };
    return result;
}

export function fromCArr<_embedded = _i_EntityRef.Cap>(_v: CArr): _.Value<_embedded> {return _.Record($arr, [_v["arity"]]);}

export function asCDict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CDict {
    let result = toCDict(v);
    if (result === void 0) throw new TypeError(`Invalid CDict: ${_.stringify(v)}`);
    return result;
}

export function toCDict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CDict {
    let result: undefined | CDict;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $dict) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromCDict<_embedded = _i_EntityRef.Cap>(_v: CDict): _.Value<_embedded> {return _.Record($dict, []);}

export function asLit<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Lit<_embedded> {
    let result = toLit(v);
    if (result === void 0) throw new TypeError(`Invalid Lit: ${_.stringify(v)}`);
    return result;
}

export function toLit<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Lit<_embedded> {
    let result: undefined | Lit<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $lit) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"value": _tmp1};};
        };
    };
    return result;
}

export function fromLit<_embedded = _i_EntityRef.Cap>(_v: Lit<_embedded>): _.Value<_embedded> {return _.Record($lit, [_v["value"]]);}

export function asPattern<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Pattern<_embedded> {
    let result = toPattern(v);
    if (result === void 0) throw new TypeError(`Invalid Pattern: ${_.stringify(v)}`);
    return result;
}

export function toPattern<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Pattern<_embedded> {
    let _tmp0: (PDiscard) | undefined;
    let result: undefined | Pattern<_embedded>;
    _tmp0 = toPDiscard(v);
    if (_tmp0 !== void 0) {result = {"_variant": "PDiscard", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (PAtom) | undefined;
        _tmp1 = toPAtom(v);
        if (_tmp1 !== void 0) {result = {"_variant": "PAtom", "value": _tmp1};};
        if (result === void 0) {
            let _tmp2: (PEmbedded) | undefined;
            _tmp2 = toPEmbedded(v);
            if (_tmp2 !== void 0) {result = {"_variant": "PEmbedded", "value": _tmp2};};
            if (result === void 0) {
                let _tmp3: (PBind<_embedded>) | undefined;
                _tmp3 = toPBind(v);
                if (_tmp3 !== void 0) {result = {"_variant": "PBind", "value": _tmp3};};
                if (result === void 0) {
                    let _tmp4: (PAnd<_embedded>) | undefined;
                    _tmp4 = toPAnd(v);
                    if (_tmp4 !== void 0) {result = {"_variant": "PAnd", "value": _tmp4};};
                    if (result === void 0) {
                        let _tmp5: (PNot<_embedded>) | undefined;
                        _tmp5 = toPNot(v);
                        if (_tmp5 !== void 0) {result = {"_variant": "PNot", "value": _tmp5};};
                        if (result === void 0) {
                            let _tmp6: (Lit<_embedded>) | undefined;
                            _tmp6 = toLit(v);
                            if (_tmp6 !== void 0) {result = {"_variant": "Lit", "value": _tmp6};};
                            if (result === void 0) {
                                let _tmp7: (PCompound<_embedded>) | undefined;
                                _tmp7 = toPCompound(v);
                                if (_tmp7 !== void 0) {result = {"_variant": "PCompound", "value": _tmp7};};
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromPattern<_embedded = _i_EntityRef.Cap>(_v: Pattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "PDiscard": {return fromPDiscard<_embedded>(_v.value);};
        case "PAtom": {return fromPAtom<_embedded>(_v.value);};
        case "PEmbedded": {return fromPEmbedded<_embedded>(_v.value);};
        case "PBind": {return fromPBind<_embedded>(_v.value);};
        case "PAnd": {return fromPAnd<_embedded>(_v.value);};
        case "PNot": {return fromPNot<_embedded>(_v.value);};
        case "Lit": {return fromLit<_embedded>(_v.value);};
        case "PCompound": {return fromPCompound<_embedded>(_v.value);};
    };
}

export function asPDiscard<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PDiscard {
    let result = toPDiscard(v);
    if (result === void 0) throw new TypeError(`Invalid PDiscard: ${_.stringify(v)}`);
    return result;
}

export function toPDiscard<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PDiscard {
    let result: undefined | PDiscard;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $_) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromPDiscard<_embedded = _i_EntityRef.Cap>(_v: PDiscard): _.Value<_embedded> {return _.Record($_, []);}

export function asPAtom<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PAtom {
    let result = toPAtom(v);
    if (result === void 0) throw new TypeError(`Invalid PAtom: ${_.stringify(v)}`);
    return result;
}

export function toPAtom<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PAtom {
    let _tmp0: (null) | undefined;
    let result: undefined | PAtom;
    _tmp0 = _.is(v, $Boolean) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "Boolean"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $Float) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "Float"};};
        if (result === void 0) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v, $Double) ? null : void 0;
            if (_tmp2 !== void 0) {result = {"_variant": "Double"};};
            if (result === void 0) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v, $SignedInteger) ? null : void 0;
                if (_tmp3 !== void 0) {result = {"_variant": "SignedInteger"};};
                if (result === void 0) {
                    let _tmp4: (null) | undefined;
                    _tmp4 = _.is(v, $String) ? null : void 0;
                    if (_tmp4 !== void 0) {result = {"_variant": "String"};};
                    if (result === void 0) {
                        let _tmp5: (null) | undefined;
                        _tmp5 = _.is(v, $ByteString) ? null : void 0;
                        if (_tmp5 !== void 0) {result = {"_variant": "ByteString"};};
                        if (result === void 0) {
                            let _tmp6: (null) | undefined;
                            _tmp6 = _.is(v, $Symbol) ? null : void 0;
                            if (_tmp6 !== void 0) {result = {"_variant": "Symbol"};};
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromPAtom<_embedded = _i_EntityRef.Cap>(_v: PAtom): _.Value<_embedded> {
    switch (_v._variant) {
        case "Boolean": {return $Boolean;};
        case "Float": {return $Float;};
        case "Double": {return $Double;};
        case "SignedInteger": {return $SignedInteger;};
        case "String": {return $String;};
        case "ByteString": {return $ByteString;};
        case "Symbol": {return $Symbol;};
    };
}

export function asPEmbedded<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PEmbedded {
    let result = toPEmbedded(v);
    if (result === void 0) throw new TypeError(`Invalid PEmbedded: ${_.stringify(v)}`);
    return result;
}

export function toPEmbedded<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PEmbedded {
    let _tmp0: (null) | undefined;
    let result: undefined | PEmbedded;
    _tmp0 = _.is(v, $Embedded) ? null : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromPEmbedded<_embedded = _i_EntityRef.Cap>(_v: PEmbedded): _.Value<_embedded> {return $Embedded;}

export function asPBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PBind<_embedded> {
    let result = toPBind(v);
    if (result === void 0) throw new TypeError(`Invalid PBind: ${_.stringify(v)}`);
    return result;
}

export function toPBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PBind<_embedded> {
    let result: undefined | PBind<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $bind) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[0]);
            if (_tmp1 !== void 0) {result = {"pattern": _tmp1};};
        };
    };
    return result;
}

export function fromPBind<_embedded = _i_EntityRef.Cap>(_v: PBind<_embedded>): _.Value<_embedded> {return _.Record($bind, [fromPattern<_embedded>(_v["pattern"])]);}

export function asPAnd<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PAnd<_embedded> {
    let result = toPAnd(v);
    if (result === void 0) throw new TypeError(`Invalid PAnd: ${_.stringify(v)}`);
    return result;
}

export function toPAnd<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PAnd<_embedded> {
    let result: undefined | PAnd<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $and) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Array<Pattern<_embedded>>) | undefined;
            _tmp1 = void 0;
            if (_.Array.isArray(v[0])) {
                _tmp1 = [];
                for (const _tmp2 of v[0]) {
                    let _tmp3: (Pattern<_embedded>) | undefined;
                    _tmp3 = toPattern(_tmp2);
                    if (_tmp3 !== void 0) {_tmp1.push(_tmp3); continue;};
                    _tmp1 = void 0;
                    break;
                };
            };
            if (_tmp1 !== void 0) {result = {"patterns": _tmp1};};
        };
    };
    return result;
}

export function fromPAnd<_embedded = _i_EntityRef.Cap>(_v: PAnd<_embedded>): _.Value<_embedded> {return _.Record($and, [_v["patterns"].map(v => fromPattern<_embedded>(v))]);}

export function asPNot<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PNot<_embedded> {
    let result = toPNot(v);
    if (result === void 0) throw new TypeError(`Invalid PNot: ${_.stringify(v)}`);
    return result;
}

export function toPNot<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PNot<_embedded> {
    let result: undefined | PNot<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $not) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[0]);
            if (_tmp1 !== void 0) {result = {"pattern": _tmp1};};
        };
    };
    return result;
}

export function fromPNot<_embedded = _i_EntityRef.Cap>(_v: PNot<_embedded>): _.Value<_embedded> {return _.Record($not, [fromPattern<_embedded>(_v["pattern"])]);}

export function asPCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PCompound<_embedded> {
    let result = toPCompound(v);
    if (result === void 0) throw new TypeError(`Invalid PCompound: ${_.stringify(v)}`);
    return result;
}

export function toPCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PCompound<_embedded> {
    let result: undefined | PCompound<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $compound) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (ConstructorSpec<_embedded>) | undefined;
            _tmp1 = toConstructorSpec(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (PCompoundMembers<_embedded>) | undefined;
                _tmp2 = toPCompoundMembers(v[1]);
                if (_tmp2 !== void 0) {result = {"ctor": _tmp1, "members": _tmp2};};
            };
        };
    };
    return result;
}

export function fromPCompound<_embedded = _i_EntityRef.Cap>(_v: PCompound<_embedded>): _.Value<_embedded> {
    return _.Record(
        $compound,
        [
            fromConstructorSpec<_embedded>(_v["ctor"]),
            fromPCompoundMembers<_embedded>(_v["members"])
        ]
    );
}

export function asPCompoundMembers<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): PCompoundMembers<_embedded> {
    let result = toPCompoundMembers(v);
    if (result === void 0) throw new TypeError(`Invalid PCompoundMembers: ${_.stringify(v)}`);
    return result;
}

export function toPCompoundMembers<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | PCompoundMembers<_embedded> {
    let _tmp0: (_.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>) | undefined;
    let result: undefined | PCompoundMembers<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (_.Value<_embedded>) | undefined;
            _tmp3 = _tmp2;
            if (_tmp3 !== void 0) {
                let _tmp4: (Pattern<_embedded>) | undefined;
                _tmp4 = toPattern(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromPCompoundMembers<_embedded = _i_EntityRef.Cap>(_v: PCompoundMembers<_embedded>): _.Value<_embedded> {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [k, fromPattern<_embedded>(v)]));
}

export function asTemplate<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Template<_embedded> {
    let result = toTemplate(v);
    if (result === void 0) throw new TypeError(`Invalid Template: ${_.stringify(v)}`);
    return result;
}

export function toTemplate<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Template<_embedded> {
    let _tmp0: (TAttenuate<_embedded>) | undefined;
    let result: undefined | Template<_embedded>;
    _tmp0 = toTAttenuate(v);
    if (_tmp0 !== void 0) {result = {"_variant": "TAttenuate", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (TRef) | undefined;
        _tmp1 = toTRef(v);
        if (_tmp1 !== void 0) {result = {"_variant": "TRef", "value": _tmp1};};
        if (result === void 0) {
            let _tmp2: (Lit<_embedded>) | undefined;
            _tmp2 = toLit(v);
            if (_tmp2 !== void 0) {result = {"_variant": "Lit", "value": _tmp2};};
            if (result === void 0) {
                let _tmp3: (TCompound<_embedded>) | undefined;
                _tmp3 = toTCompound(v);
                if (_tmp3 !== void 0) {result = {"_variant": "TCompound", "value": _tmp3};};
            };
        };
    };
    return result;
}

export function fromTemplate<_embedded = _i_EntityRef.Cap>(_v: Template<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "TAttenuate": {return fromTAttenuate<_embedded>(_v.value);};
        case "TRef": {return fromTRef<_embedded>(_v.value);};
        case "Lit": {return fromLit<_embedded>(_v.value);};
        case "TCompound": {return fromTCompound<_embedded>(_v.value);};
    };
}

export function asTAttenuate<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TAttenuate<_embedded> {
    let result = toTAttenuate(v);
    if (result === void 0) throw new TypeError(`Invalid TAttenuate: ${_.stringify(v)}`);
    return result;
}

export function toTAttenuate<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TAttenuate<_embedded> {
    let result: undefined | TAttenuate<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $attenuate) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Template<_embedded>) | undefined;
            _tmp1 = toTemplate(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (Attenuation<_embedded>) | undefined;
                _tmp2 = toAttenuation(v[1]);
                if (_tmp2 !== void 0) {result = {"template": _tmp1, "attenuation": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTAttenuate<_embedded = _i_EntityRef.Cap>(_v: TAttenuate<_embedded>): _.Value<_embedded> {
    return _.Record(
        $attenuate,
        [
            fromTemplate<_embedded>(_v["template"]),
            fromAttenuation<_embedded>(_v["attenuation"])
        ]
    );
}

export function asTRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TRef {
    let result = toTRef(v);
    if (result === void 0) throw new TypeError(`Invalid TRef: ${_.stringify(v)}`);
    return result;
}

export function toTRef<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TRef {
    let result: undefined | TRef;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $ref) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"binding": _tmp1};};
        };
    };
    return result;
}

export function fromTRef<_embedded = _i_EntityRef.Cap>(_v: TRef): _.Value<_embedded> {return _.Record($ref, [_v["binding"]]);}

export function asTCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TCompound<_embedded> {
    let result = toTCompound(v);
    if (result === void 0) throw new TypeError(`Invalid TCompound: ${_.stringify(v)}`);
    return result;
}

export function toTCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TCompound<_embedded> {
    let result: undefined | TCompound<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $compound) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (ConstructorSpec<_embedded>) | undefined;
            _tmp1 = toConstructorSpec(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (TCompoundMembers<_embedded>) | undefined;
                _tmp2 = toTCompoundMembers(v[1]);
                if (_tmp2 !== void 0) {result = {"ctor": _tmp1, "members": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTCompound<_embedded = _i_EntityRef.Cap>(_v: TCompound<_embedded>): _.Value<_embedded> {
    return _.Record(
        $compound,
        [
            fromConstructorSpec<_embedded>(_v["ctor"]),
            fromTCompoundMembers<_embedded>(_v["members"])
        ]
    );
}

export function asTCompoundMembers<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TCompoundMembers<_embedded> {
    let result = toTCompoundMembers(v);
    if (result === void 0) throw new TypeError(`Invalid TCompoundMembers: ${_.stringify(v)}`);
    return result;
}

export function toTCompoundMembers<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TCompoundMembers<_embedded> {
    let _tmp0: (_.KeyedDictionary<_.Value<_embedded>, Template<_embedded>, _embedded>) | undefined;
    let result: undefined | TCompoundMembers<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (_.Value<_embedded>) | undefined;
            _tmp3 = _tmp2;
            if (_tmp3 !== void 0) {
                let _tmp4: (Template<_embedded>) | undefined;
                _tmp4 = toTemplate(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromTCompoundMembers<_embedded = _i_EntityRef.Cap>(_v: TCompoundMembers<_embedded>): _.Value<_embedded> {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [k, fromTemplate<_embedded>(v)]));
}

