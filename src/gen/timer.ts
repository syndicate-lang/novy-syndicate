import * as _ from "@preserves/core";

export const $absolute = Symbol.for("absolute");
export const $clear = Symbol.for("clear");
export const $relative = Symbol.for("relative");
export const __lit0 = Symbol.for("set-timer");
export const __lit1 = Symbol.for("timer-expired");
export const __lit5 = Symbol.for("later-than");

export type SetTimer<_embedded = _.GenericEmbedded> = {"label": _.Value<_embedded>, "msecs": number, "kind": TimerKind};

export type TimerExpired<_embedded = _.GenericEmbedded> = {"label": _.Value<_embedded>, "msecs": number};

export type TimerKind = ({"_variant": "relative"} | {"_variant": "absolute"} | {"_variant": "clear"});

export type LaterThan = {"msecs": number};


export function SetTimer<_embedded = _.GenericEmbedded>(
    {label, msecs, kind}: {label: _.Value<_embedded>, msecs: number, kind: TimerKind}
): SetTimer<_embedded> {return {"label": label, "msecs": msecs, "kind": kind};}

export function TimerExpired<_embedded = _.GenericEmbedded>({label, msecs}: {label: _.Value<_embedded>, msecs: number}): TimerExpired<_embedded> {return {"label": label, "msecs": msecs};}

export namespace TimerKind {
    export function relative(): TimerKind {return {"_variant": "relative"};};
    export function absolute(): TimerKind {return {"_variant": "absolute"};};
    export function clear(): TimerKind {return {"_variant": "clear"};};
}

export function LaterThan(msecs: number): LaterThan {return {"msecs": msecs};}

export function asSetTimer<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): SetTimer<_embedded> {
    let result = toSetTimer(v);
    if (result === void 0) throw new TypeError(`Invalid SetTimer: ${_.stringify(v)}`);
    return result;
}

export function toSetTimer<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SetTimer<_embedded> {
    let result: undefined | SetTimer<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = _.Float.isDouble(v[1]) ? v[1].value : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (TimerKind) | undefined;
                    _tmp3 = toTimerKind(v[2]);
                    if (_tmp3 !== void 0) {result = {"label": _tmp1, "msecs": _tmp2, "kind": _tmp3};};
                };
            };
        };
    };
    return result;
}

export function fromSetTimer<_embedded = _.GenericEmbedded>(_v: SetTimer<_embedded>): _.Value<_embedded> {
    return _.Record(
        __lit0,
        [_v["label"], _.Double(_v["msecs"]), fromTimerKind<_embedded>(_v["kind"])]
    );
}

export function asTimerExpired<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): TimerExpired<_embedded> {
    let result = toTimerExpired(v);
    if (result === void 0) throw new TypeError(`Invalid TimerExpired: ${_.stringify(v)}`);
    return result;
}

export function toTimerExpired<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | TimerExpired<_embedded> {
    let result: undefined | TimerExpired<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit1) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = _.Float.isDouble(v[1]) ? v[1].value : void 0;
                if (_tmp2 !== void 0) {result = {"label": _tmp1, "msecs": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTimerExpired<_embedded = _.GenericEmbedded>(_v: TimerExpired<_embedded>): _.Value<_embedded> {return _.Record(__lit1, [_v["label"], _.Double(_v["msecs"])]);}

export function asTimerKind<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): TimerKind {
    let result = toTimerKind(v);
    if (result === void 0) throw new TypeError(`Invalid TimerKind: ${_.stringify(v)}`);
    return result;
}

export function toTimerKind<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | TimerKind {
    let _tmp0: (null) | undefined;
    let result: undefined | TimerKind;
    _tmp0 = _.is(v, $relative) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "relative"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $absolute) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "absolute"};};
        if (result === void 0) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v, $clear) ? null : void 0;
            if (_tmp2 !== void 0) {result = {"_variant": "clear"};};
        };
    };
    return result;
}

export function fromTimerKind<_embedded = _.GenericEmbedded>(_v: TimerKind): _.Value<_embedded> {
    switch (_v._variant) {
        case "relative": {return $relative;};
        case "absolute": {return $absolute;};
        case "clear": {return $clear;};
    };
}

export function asLaterThan<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): LaterThan {
    let result = toLaterThan(v);
    if (result === void 0) throw new TypeError(`Invalid LaterThan: ${_.stringify(v)}`);
    return result;
}

export function toLaterThan<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | LaterThan {
    let result: undefined | LaterThan;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit5) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = _.Float.isDouble(v[0]) ? v[0].value : void 0;
            if (_tmp1 !== void 0) {result = {"msecs": _tmp1};};
        };
    };
    return result;
}

export function fromLaterThan<_embedded = _.GenericEmbedded>(_v: LaterThan): _.Value<_embedded> {return _.Record(__lit5, [_.Double(_v["msecs"])]);}

