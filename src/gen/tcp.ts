import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const __lit0 = Symbol.for("tcp-remote");
export const __lit1 = Symbol.for("tcp-local");
export const __lit2 = Symbol.for("tcp-peer");

export type _embedded = _i_EntityRef.Cap;

export type TcpRemote = {"host": string, "port": number};

export type TcpLocal = {"host": string, "port": number};

export type TcpPeerInfo<_embedded = _i_EntityRef.Cap> = {"handle": _embedded, "local": TcpLocal, "remote": TcpRemote};


export function TcpRemote({host, port}: {host: string, port: number}): TcpRemote {return {"host": host, "port": port};}

export function TcpLocal({host, port}: {host: string, port: number}): TcpLocal {return {"host": host, "port": port};}

export function TcpPeerInfo<_embedded = _i_EntityRef.Cap>(
    {handle, local, remote}: {handle: _embedded, local: TcpLocal, remote: TcpRemote}
): TcpPeerInfo<_embedded> {return {"handle": handle, "local": local, "remote": remote};}

export function asTcpRemote<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TcpRemote {
    let result = toTcpRemote(v);
    if (result === void 0) throw new TypeError(`Invalid TcpRemote: ${_.stringify(v)}`);
    return result;
}

export function toTcpRemote<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TcpRemote {
    let result: undefined | TcpRemote;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"host": _tmp1, "port": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTcpRemote<_embedded = _i_EntityRef.Cap>(_v: TcpRemote): _.Value<_embedded> {return _.Record(__lit0, [_v["host"], _v["port"]]);}

export function asTcpLocal<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TcpLocal {
    let result = toTcpLocal(v);
    if (result === void 0) throw new TypeError(`Invalid TcpLocal: ${_.stringify(v)}`);
    return result;
}

export function toTcpLocal<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TcpLocal {
    let result: undefined | TcpLocal;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit1) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"host": _tmp1, "port": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTcpLocal<_embedded = _i_EntityRef.Cap>(_v: TcpLocal): _.Value<_embedded> {return _.Record(__lit1, [_v["host"], _v["port"]]);}

export function asTcpPeerInfo<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): TcpPeerInfo<_embedded> {
    let result = toTcpPeerInfo(v);
    if (result === void 0) throw new TypeError(`Invalid TcpPeerInfo: ${_.stringify(v)}`);
    return result;
}

export function toTcpPeerInfo<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | TcpPeerInfo<_embedded> {
    let result: undefined | TcpPeerInfo<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit2) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (TcpLocal) | undefined;
                _tmp2 = toTcpLocal(v[1]);
                if (_tmp2 !== void 0) {
                    let _tmp3: (TcpRemote) | undefined;
                    _tmp3 = toTcpRemote(v[2]);
                    if (_tmp3 !== void 0) {result = {"handle": _tmp1, "local": _tmp2, "remote": _tmp3};};
                };
            };
        };
    };
    return result;
}

export function fromTcpPeerInfo<_embedded = _i_EntityRef.Cap>(_v: TcpPeerInfo<_embedded>): _.Value<_embedded> {
    return _.Record(
        __lit2,
        [
            _.embed(_v["handle"]),
            fromTcpLocal<_embedded>(_v["local"]),
            fromTcpRemote<_embedded>(_v["remote"])
        ]
    );
}

