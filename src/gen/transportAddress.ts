import * as _ from "@preserves/core";

export const $stdio = Symbol.for("stdio");
export const $tcp = Symbol.for("tcp");
export const $unix = Symbol.for("unix");
export const $ws = Symbol.for("ws");

export type Tcp = {"host": string, "port": number};

export type Unix = {"path": string};

export type WebSocket = {"url": string};

export type Stdio = null;


export function Tcp({host, port}: {host: string, port: number}): Tcp {return {"host": host, "port": port};}

export function Unix(path: string): Unix {return {"path": path};}

export function WebSocket(url: string): WebSocket {return {"url": url};}

export function Stdio(): Stdio {return null;}

export function asTcp<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Tcp {
    let result = toTcp(v);
    if (result === void 0) throw new TypeError(`Invalid Tcp: ${_.stringify(v)}`);
    return result;
}

export function toTcp<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Tcp {
    let result: undefined | Tcp;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $tcp) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"host": _tmp1, "port": _tmp2};};
            };
        };
    };
    return result;
}

export function fromTcp<_embedded = _.GenericEmbedded>(_v: Tcp): _.Value<_embedded> {return _.Record($tcp, [_v["host"], _v["port"]]);}

export function asUnix<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Unix {
    let result = toUnix(v);
    if (result === void 0) throw new TypeError(`Invalid Unix: ${_.stringify(v)}`);
    return result;
}

export function toUnix<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Unix {
    let result: undefined | Unix;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $unix) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"path": _tmp1};};
        };
    };
    return result;
}

export function fromUnix<_embedded = _.GenericEmbedded>(_v: Unix): _.Value<_embedded> {return _.Record($unix, [_v["path"]]);}

export function asWebSocket<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): WebSocket {
    let result = toWebSocket(v);
    if (result === void 0) throw new TypeError(`Invalid WebSocket: ${_.stringify(v)}`);
    return result;
}

export function toWebSocket<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | WebSocket {
    let result: undefined | WebSocket;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $ws) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"url": _tmp1};};
        };
    };
    return result;
}

export function fromWebSocket<_embedded = _.GenericEmbedded>(_v: WebSocket): _.Value<_embedded> {return _.Record($ws, [_v["url"]]);}

export function asStdio<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Stdio {
    let result = toStdio(v);
    if (result === void 0) throw new TypeError(`Invalid Stdio: ${_.stringify(v)}`);
    return result;
}

export function toStdio<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Stdio {
    let result: undefined | Stdio;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $stdio) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromStdio<_embedded = _.GenericEmbedded>(_v: Stdio): _.Value<_embedded> {return _.Record($stdio, []);}

