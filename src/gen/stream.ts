import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $bytes = Symbol.for("bytes");
export const $credit = Symbol.for("credit");
export const $crlf = Symbol.for("crlf");
export const $data = Symbol.for("data");
export const $eof = Symbol.for("eof");
export const $error = Symbol.for("error");
export const $lf = Symbol.for("lf");
export const $object = Symbol.for("object");
export const $packet = Symbol.for("packet");
export const $sink = Symbol.for("sink");
export const $source = Symbol.for("source");
export const $unbounded = Symbol.for("unbounded");
export const __lit0 = Symbol.for("stream-connection");
export const __lit1 = Symbol.for("stream-listener-ready");
export const __lit2 = Symbol.for("stream-listener-error");

export type _embedded = _i_EntityRef.Cap;

export type StreamConnection<_embedded = _i_EntityRef.Cap> = {"source": _embedded, "sink": _embedded, "spec": _.Value<_embedded>};

export type StreamListenerReady<_embedded = _i_EntityRef.Cap> = {"spec": _.Value<_embedded>};

export type StreamListenerError<_embedded = _i_EntityRef.Cap> = {"spec": _.Value<_embedded>, "message": string};

export type StreamError = {"message": string};

export type Source<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "sink", "controller": _embedded} |
    {"_variant": "StreamError", "value": StreamError} |
    {"_variant": "credit", "amount": CreditAmount, "mode": Mode<_embedded>}
);

export type Sink<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "source", "controller": _embedded} |
    {"_variant": "StreamError", "value": StreamError} |
    {"_variant": "data", "payload": _.Value<_embedded>, "mode": Mode<_embedded>} |
    {"_variant": "eof"}
);

export type CreditAmount = ({"_variant": "count", "value": number} | {"_variant": "unbounded"});

export type Mode<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "bytes"} |
    {"_variant": "lines", "value": LineMode} |
    {"_variant": "packet", "size": number} |
    {"_variant": "object", "description": _.Value<_embedded>}
);

export type LineMode = ({"_variant": "lf"} | {"_variant": "crlf"});


export function StreamConnection<_embedded = _i_EntityRef.Cap>(
    {source, sink, spec}: {source: _embedded, sink: _embedded, spec: _.Value<_embedded>}
): StreamConnection<_embedded> {return {"source": source, "sink": sink, "spec": spec};}

export function StreamListenerReady<_embedded = _i_EntityRef.Cap>(spec: _.Value<_embedded>): StreamListenerReady<_embedded> {return {"spec": spec};}

export function StreamListenerError<_embedded = _i_EntityRef.Cap>({spec, message}: {spec: _.Value<_embedded>, message: string}): StreamListenerError<_embedded> {return {"spec": spec, "message": message};}

export function StreamError(message: string): StreamError {return {"message": message};}

export namespace Source {
    export function sink<_embedded = _i_EntityRef.Cap>(controller: _embedded): Source<_embedded> {return {"_variant": "sink", "controller": controller};};
    export function StreamError<_embedded = _i_EntityRef.Cap>(value: StreamError): Source<_embedded> {return {"_variant": "StreamError", "value": value};};
    export function credit<_embedded = _i_EntityRef.Cap>({amount, mode}: {amount: CreditAmount, mode: Mode<_embedded>}): Source<_embedded> {return {"_variant": "credit", "amount": amount, "mode": mode};};
}

export namespace Sink {
    export function source<_embedded = _i_EntityRef.Cap>(controller: _embedded): Sink<_embedded> {return {"_variant": "source", "controller": controller};};
    export function StreamError<_embedded = _i_EntityRef.Cap>(value: StreamError): Sink<_embedded> {return {"_variant": "StreamError", "value": value};};
    export function data<_embedded = _i_EntityRef.Cap>({payload, mode}: {payload: _.Value<_embedded>, mode: Mode<_embedded>}): Sink<_embedded> {return {"_variant": "data", "payload": payload, "mode": mode};};
    export function eof<_embedded = _i_EntityRef.Cap>(): Sink<_embedded> {return {"_variant": "eof"};};
}

export namespace CreditAmount {
    export function count(value: number): CreditAmount {return {"_variant": "count", "value": value};};
    export function unbounded(): CreditAmount {return {"_variant": "unbounded"};};
}

export namespace Mode {
    export function bytes<_embedded = _i_EntityRef.Cap>(): Mode<_embedded> {return {"_variant": "bytes"};};
    export function lines<_embedded = _i_EntityRef.Cap>(value: LineMode): Mode<_embedded> {return {"_variant": "lines", "value": value};};
    export function packet<_embedded = _i_EntityRef.Cap>(size: number): Mode<_embedded> {return {"_variant": "packet", "size": size};};
    export function object<_embedded = _i_EntityRef.Cap>(description: _.Value<_embedded>): Mode<_embedded> {return {"_variant": "object", "description": description};};
}

export namespace LineMode {
    export function lf(): LineMode {return {"_variant": "lf"};};
    export function crlf(): LineMode {return {"_variant": "crlf"};};
}

export function asStreamConnection<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): StreamConnection<_embedded> {
    let result = toStreamConnection(v);
    if (result === void 0) throw new TypeError(`Invalid StreamConnection: ${_.stringify(v)}`);
    return result;
}

export function toStreamConnection<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | StreamConnection<_embedded> {
    let result: undefined | StreamConnection<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (_.Value<_embedded>) | undefined;
                    _tmp3 = v[2];
                    if (_tmp3 !== void 0) {result = {"source": _tmp1, "sink": _tmp2, "spec": _tmp3};};
                };
            };
        };
    };
    return result;
}

export function fromStreamConnection<_embedded = _i_EntityRef.Cap>(_v: StreamConnection<_embedded>): _.Value<_embedded> {
    return _.Record(__lit0, [_.embed(_v["source"]), _.embed(_v["sink"]), _v["spec"]]);
}

export function asStreamListenerReady<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): StreamListenerReady<_embedded> {
    let result = toStreamListenerReady(v);
    if (result === void 0) throw new TypeError(`Invalid StreamListenerReady: ${_.stringify(v)}`);
    return result;
}

export function toStreamListenerReady<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | StreamListenerReady<_embedded> {
    let result: undefined | StreamListenerReady<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit1) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"spec": _tmp1};};
        };
    };
    return result;
}

export function fromStreamListenerReady<_embedded = _i_EntityRef.Cap>(_v: StreamListenerReady<_embedded>): _.Value<_embedded> {return _.Record(__lit1, [_v["spec"]]);}

export function asStreamListenerError<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): StreamListenerError<_embedded> {
    let result = toStreamListenerError(v);
    if (result === void 0) throw new TypeError(`Invalid StreamListenerError: ${_.stringify(v)}`);
    return result;
}

export function toStreamListenerError<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | StreamListenerError<_embedded> {
    let result: undefined | StreamListenerError<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit2) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"spec": _tmp1, "message": _tmp2};};
            };
        };
    };
    return result;
}

export function fromStreamListenerError<_embedded = _i_EntityRef.Cap>(_v: StreamListenerError<_embedded>): _.Value<_embedded> {return _.Record(__lit2, [_v["spec"], _v["message"]]);}

export function asStreamError<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): StreamError {
    let result = toStreamError(v);
    if (result === void 0) throw new TypeError(`Invalid StreamError: ${_.stringify(v)}`);
    return result;
}

export function toStreamError<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | StreamError {
    let result: undefined | StreamError;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $error) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"message": _tmp1};};
        };
    };
    return result;
}

export function fromStreamError<_embedded = _i_EntityRef.Cap>(_v: StreamError): _.Value<_embedded> {return _.Record($error, [_v["message"]]);}

export function asSource<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Source<_embedded> {
    let result = toSource(v);
    if (result === void 0) throw new TypeError(`Invalid Source: ${_.stringify(v)}`);
    return result;
}

export function toSource<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Source<_embedded> {
    let result: undefined | Source<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $sink) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {result = {"_variant": "sink", "controller": _tmp1};};
        };
    };
    if (result === void 0) {
        let _tmp2: (StreamError) | undefined;
        _tmp2 = toStreamError(v);
        if (_tmp2 !== void 0) {result = {"_variant": "StreamError", "value": _tmp2};};
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v.label, $credit) ? null : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (CreditAmount) | undefined;
                    _tmp4 = toCreditAmount(v[0]);
                    if (_tmp4 !== void 0) {
                        let _tmp5: (Mode<_embedded>) | undefined;
                        _tmp5 = toMode(v[1]);
                        if (_tmp5 !== void 0) {result = {"_variant": "credit", "amount": _tmp4, "mode": _tmp5};};
                    };
                };
            };
        };
    };
    return result;
}

export function fromSource<_embedded = _i_EntityRef.Cap>(_v: Source<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "sink": {return _.Record($sink, [_.embed(_v["controller"])]);};
        case "StreamError": {return fromStreamError<_embedded>(_v.value);};
        case "credit": {
            return _.Record(
                $credit,
                [fromCreditAmount<_embedded>(_v["amount"]), fromMode<_embedded>(_v["mode"])]
            );
        };
    };
}

export function asSink<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Sink<_embedded> {
    let result = toSink(v);
    if (result === void 0) throw new TypeError(`Invalid Sink: ${_.stringify(v)}`);
    return result;
}

export function toSink<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Sink<_embedded> {
    let result: undefined | Sink<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $source) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {result = {"_variant": "source", "controller": _tmp1};};
        };
    };
    if (result === void 0) {
        let _tmp2: (StreamError) | undefined;
        _tmp2 = toStreamError(v);
        if (_tmp2 !== void 0) {result = {"_variant": "StreamError", "value": _tmp2};};
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v.label, $data) ? null : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (_.Value<_embedded>) | undefined;
                    _tmp4 = v[0];
                    if (_tmp4 !== void 0) {
                        let _tmp5: (Mode<_embedded>) | undefined;
                        _tmp5 = toMode(v[1]);
                        if (_tmp5 !== void 0) {result = {"_variant": "data", "payload": _tmp4, "mode": _tmp5};};
                    };
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp6: (null) | undefined;
                    _tmp6 = _.is(v.label, $eof) ? null : void 0;
                    if (_tmp6 !== void 0) {result = {"_variant": "eof"};};
                };
            };
        };
    };
    return result;
}

export function fromSink<_embedded = _i_EntityRef.Cap>(_v: Sink<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "source": {return _.Record($source, [_.embed(_v["controller"])]);};
        case "StreamError": {return fromStreamError<_embedded>(_v.value);};
        case "data": {return _.Record($data, [_v["payload"], fromMode<_embedded>(_v["mode"])]);};
        case "eof": {return _.Record($eof, []);};
    };
}

export function asCreditAmount<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CreditAmount {
    let result = toCreditAmount(v);
    if (result === void 0) throw new TypeError(`Invalid CreditAmount: ${_.stringify(v)}`);
    return result;
}

export function toCreditAmount<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CreditAmount {
    let _tmp0: (number) | undefined;
    let result: undefined | CreditAmount;
    _tmp0 = typeof v === 'number' ? v : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "count", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $unbounded) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "unbounded"};};
    };
    return result;
}

export function fromCreditAmount<_embedded = _i_EntityRef.Cap>(_v: CreditAmount): _.Value<_embedded> {
    switch (_v._variant) {case "count": {return _v.value;}; case "unbounded": {return $unbounded;};};
}

export function asMode<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Mode<_embedded> {
    let result = toMode(v);
    if (result === void 0) throw new TypeError(`Invalid Mode: ${_.stringify(v)}`);
    return result;
}

export function toMode<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Mode<_embedded> {
    let _tmp0: (null) | undefined;
    let result: undefined | Mode<_embedded>;
    _tmp0 = _.is(v, $bytes) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "bytes"};};
    if (result === void 0) {
        let _tmp1: (LineMode) | undefined;
        _tmp1 = toLineMode(v);
        if (_tmp1 !== void 0) {result = {"_variant": "lines", "value": _tmp1};};
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp2: (null) | undefined;
                _tmp2 = _.is(v.label, $packet) ? null : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (number) | undefined;
                    _tmp3 = typeof v[0] === 'number' ? v[0] : void 0;
                    if (_tmp3 !== void 0) {result = {"_variant": "packet", "size": _tmp3};};
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp4: (null) | undefined;
                    _tmp4 = _.is(v.label, $object) ? null : void 0;
                    if (_tmp4 !== void 0) {
                        let _tmp5: (_.Value<_embedded>) | undefined;
                        _tmp5 = v[0];
                        if (_tmp5 !== void 0) {result = {"_variant": "object", "description": _tmp5};};
                    };
                };
            };
        };
    };
    return result;
}

export function fromMode<_embedded = _i_EntityRef.Cap>(_v: Mode<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "bytes": {return $bytes;};
        case "lines": {return fromLineMode<_embedded>(_v.value);};
        case "packet": {return _.Record($packet, [_v["size"]]);};
        case "object": {return _.Record($object, [_v["description"]]);};
    };
}

export function asLineMode<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): LineMode {
    let result = toLineMode(v);
    if (result === void 0) throw new TypeError(`Invalid LineMode: ${_.stringify(v)}`);
    return result;
}

export function toLineMode<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | LineMode {
    let _tmp0: (null) | undefined;
    let result: undefined | LineMode;
    _tmp0 = _.is(v, $lf) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "lf"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $crlf) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "crlf"};};
    };
    return result;
}

export function fromLineMode<_embedded = _i_EntityRef.Cap>(_v: LineMode): _.Value<_embedded> {
    switch (_v._variant) {case "lf": {return $lf;}; case "crlf": {return $crlf;};};
}

