import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $Present = Symbol.for("Present");
export const $Says = Symbol.for("Says");

export type _embedded = _i_EntityRef.Cap;

export type Present = {"username": string};

export type Says = {"who": string, "what": string};


export function Present(username: string): Present {return {"username": username};}

export function Says({who, what}: {who: string, what: string}): Says {return {"who": who, "what": what};}

export function asPresent<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Present {
    let result = toPresent(v);
    if (result === void 0) throw new TypeError(`Invalid Present: ${_.stringify(v)}`);
    return result;
}

export function toPresent<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Present {
    let result: undefined | Present;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $Present) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"username": _tmp1};};
        };
    };
    return result;
}

export function fromPresent<_embedded = _i_EntityRef.Cap>(_v: Present): _.Value<_embedded> {return _.Record($Present, [_v["username"]]);}

export function asSays<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Says {
    let result = toSays(v);
    if (result === void 0) throw new TypeError(`Invalid Says: ${_.stringify(v)}`);
    return result;
}

export function toSays<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Says {
    let result: undefined | Says;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $Says) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"who": _tmp1, "what": _tmp2};};
            };
        };
    };
    return result;
}

export function fromSays<_embedded = _i_EntityRef.Cap>(_v: Says): _.Value<_embedded> {return _.Record($Says, [_v["who"], _v["what"]]);}

