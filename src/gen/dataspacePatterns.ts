import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $_ = Symbol.for("_");
export const $arr = Symbol.for("arr");
export const $bind = Symbol.for("bind");
export const $compound = Symbol.for("compound");
export const $dict = Symbol.for("dict");
export const $lit = Symbol.for("lit");
export const $rec = Symbol.for("rec");

export type _embedded = _i_EntityRef.Cap;

export type Pattern<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "DDiscard", "value": DDiscard} |
    {"_variant": "DBind", "value": DBind<_embedded>} |
    {"_variant": "DLit", "value": DLit<_embedded>} |
    {"_variant": "DCompound", "value": DCompound<_embedded>}
);

export type DDiscard = null;

export type DBind<_embedded = _i_EntityRef.Cap> = {"pattern": Pattern<_embedded>};

export type DLit<_embedded = _i_EntityRef.Cap> = {"value": _.Value<_embedded>};

export type DCompound<_embedded = _i_EntityRef.Cap> = (
    {
        "_variant": "rec",
        "ctor": CRec<_embedded>,
        "members": _.KeyedDictionary<number, Pattern<_embedded>, _embedded>
    } |
    {
        "_variant": "arr",
        "ctor": CArr,
        "members": _.KeyedDictionary<number, Pattern<_embedded>, _embedded>
    } |
    {
        "_variant": "dict",
        "ctor": CDict,
        "members": _.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>
    }
);

export type CRec<_embedded = _i_EntityRef.Cap> = {"label": _.Value<_embedded>, "arity": number};

export type CArr = {"arity": number};

export type CDict = null;


export namespace Pattern {
    export function DDiscard<_embedded = _i_EntityRef.Cap>(value: DDiscard): Pattern<_embedded> {return {"_variant": "DDiscard", "value": value};};
    export function DBind<_embedded = _i_EntityRef.Cap>(value: DBind<_embedded>): Pattern<_embedded> {return {"_variant": "DBind", "value": value};};
    export function DLit<_embedded = _i_EntityRef.Cap>(value: DLit<_embedded>): Pattern<_embedded> {return {"_variant": "DLit", "value": value};};
    export function DCompound<_embedded = _i_EntityRef.Cap>(value: DCompound<_embedded>): Pattern<_embedded> {return {"_variant": "DCompound", "value": value};};
}

export function DDiscard(): DDiscard {return null;}

export function DBind<_embedded = _i_EntityRef.Cap>(pattern: Pattern<_embedded>): DBind<_embedded> {return {"pattern": pattern};}

export function DLit<_embedded = _i_EntityRef.Cap>(value: _.Value<_embedded>): DLit<_embedded> {return {"value": value};}

export namespace DCompound {
    export function rec<_embedded = _i_EntityRef.Cap>(
        {ctor, members}: {
            ctor: CRec<_embedded>,
            members: _.KeyedDictionary<number, Pattern<_embedded>, _embedded>
        }
    ): DCompound<_embedded> {return {"_variant": "rec", "ctor": ctor, "members": members};};
    export function arr<_embedded = _i_EntityRef.Cap>(
        {ctor, members}: {ctor: CArr, members: _.KeyedDictionary<number, Pattern<_embedded>, _embedded>}
    ): DCompound<_embedded> {return {"_variant": "arr", "ctor": ctor, "members": members};};
    export function dict<_embedded = _i_EntityRef.Cap>(
        {ctor, members}: {
            ctor: CDict,
            members: _.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>
        }
    ): DCompound<_embedded> {return {"_variant": "dict", "ctor": ctor, "members": members};};
}

export function CRec<_embedded = _i_EntityRef.Cap>({label, arity}: {label: _.Value<_embedded>, arity: number}): CRec<_embedded> {return {"label": label, "arity": arity};}

export function CArr(arity: number): CArr {return {"arity": arity};}

export function CDict(): CDict {return null;}

export function asPattern<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Pattern<_embedded> {
    let result = toPattern(v);
    if (result === void 0) throw new TypeError(`Invalid Pattern: ${_.stringify(v)}`);
    return result;
}

export function toPattern<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Pattern<_embedded> {
    let _tmp0: (DDiscard) | undefined;
    let result: undefined | Pattern<_embedded>;
    _tmp0 = toDDiscard(v);
    if (_tmp0 !== void 0) {result = {"_variant": "DDiscard", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (DBind<_embedded>) | undefined;
        _tmp1 = toDBind(v);
        if (_tmp1 !== void 0) {result = {"_variant": "DBind", "value": _tmp1};};
        if (result === void 0) {
            let _tmp2: (DLit<_embedded>) | undefined;
            _tmp2 = toDLit(v);
            if (_tmp2 !== void 0) {result = {"_variant": "DLit", "value": _tmp2};};
            if (result === void 0) {
                let _tmp3: (DCompound<_embedded>) | undefined;
                _tmp3 = toDCompound(v);
                if (_tmp3 !== void 0) {result = {"_variant": "DCompound", "value": _tmp3};};
            };
        };
    };
    return result;
}

export function fromPattern<_embedded = _i_EntityRef.Cap>(_v: Pattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "DDiscard": {return fromDDiscard<_embedded>(_v.value);};
        case "DBind": {return fromDBind<_embedded>(_v.value);};
        case "DLit": {return fromDLit<_embedded>(_v.value);};
        case "DCompound": {return fromDCompound<_embedded>(_v.value);};
    };
}

export function asDDiscard<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): DDiscard {
    let result = toDDiscard(v);
    if (result === void 0) throw new TypeError(`Invalid DDiscard: ${_.stringify(v)}`);
    return result;
}

export function toDDiscard<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | DDiscard {
    let result: undefined | DDiscard;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $_) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromDDiscard<_embedded = _i_EntityRef.Cap>(_v: DDiscard): _.Value<_embedded> {return _.Record($_, []);}

export function asDBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): DBind<_embedded> {
    let result = toDBind(v);
    if (result === void 0) throw new TypeError(`Invalid DBind: ${_.stringify(v)}`);
    return result;
}

export function toDBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | DBind<_embedded> {
    let result: undefined | DBind<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $bind) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[0]);
            if (_tmp1 !== void 0) {result = {"pattern": _tmp1};};
        };
    };
    return result;
}

export function fromDBind<_embedded = _i_EntityRef.Cap>(_v: DBind<_embedded>): _.Value<_embedded> {return _.Record($bind, [fromPattern<_embedded>(_v["pattern"])]);}

export function asDLit<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): DLit<_embedded> {
    let result = toDLit(v);
    if (result === void 0) throw new TypeError(`Invalid DLit: ${_.stringify(v)}`);
    return result;
}

export function toDLit<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | DLit<_embedded> {
    let result: undefined | DLit<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $lit) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"value": _tmp1};};
        };
    };
    return result;
}

export function fromDLit<_embedded = _i_EntityRef.Cap>(_v: DLit<_embedded>): _.Value<_embedded> {return _.Record($lit, [_v["value"]]);}

export function asDCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): DCompound<_embedded> {
    let result = toDCompound(v);
    if (result === void 0) throw new TypeError(`Invalid DCompound: ${_.stringify(v)}`);
    return result;
}

export function toDCompound<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | DCompound<_embedded> {
    let result: undefined | DCompound<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $compound) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (CRec<_embedded>) | undefined;
            _tmp1 = toCRec(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (_.KeyedDictionary<number, Pattern<_embedded>, _embedded>) | undefined;
                _tmp2 = void 0;
                if (_.Dictionary.isDictionary<_embedded>(v[1])) {
                    _tmp2 = new _.KeyedDictionary();
                    for (const [_tmp4, _tmp3] of v[1]) {
                        let _tmp5: (number) | undefined;
                        _tmp5 = typeof _tmp4 === 'number' ? _tmp4 : void 0;
                        if (_tmp5 !== void 0) {
                            let _tmp6: (Pattern<_embedded>) | undefined;
                            _tmp6 = toPattern(_tmp3);
                            if (_tmp6 !== void 0) {_tmp2.set(_tmp5, _tmp6); continue;};
                        };
                        _tmp2 = void 0;
                        break;
                    };
                };
                if (_tmp2 !== void 0) {result = {"_variant": "rec", "ctor": _tmp1, "members": _tmp2};};
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp7: (null) | undefined;
            _tmp7 = _.is(v.label, $compound) ? null : void 0;
            if (_tmp7 !== void 0) {
                let _tmp8: (CArr) | undefined;
                _tmp8 = toCArr(v[0]);
                if (_tmp8 !== void 0) {
                    let _tmp9: (_.KeyedDictionary<number, Pattern<_embedded>, _embedded>) | undefined;
                    _tmp9 = void 0;
                    if (_.Dictionary.isDictionary<_embedded>(v[1])) {
                        _tmp9 = new _.KeyedDictionary();
                        for (const [_tmp11, _tmp10] of v[1]) {
                            let _tmp12: (number) | undefined;
                            _tmp12 = typeof _tmp11 === 'number' ? _tmp11 : void 0;
                            if (_tmp12 !== void 0) {
                                let _tmp13: (Pattern<_embedded>) | undefined;
                                _tmp13 = toPattern(_tmp10);
                                if (_tmp13 !== void 0) {_tmp9.set(_tmp12, _tmp13); continue;};
                            };
                            _tmp9 = void 0;
                            break;
                        };
                    };
                    if (_tmp9 !== void 0) {result = {"_variant": "arr", "ctor": _tmp8, "members": _tmp9};};
                };
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp14: (null) | undefined;
                _tmp14 = _.is(v.label, $compound) ? null : void 0;
                if (_tmp14 !== void 0) {
                    let _tmp15: (CDict) | undefined;
                    _tmp15 = toCDict(v[0]);
                    if (_tmp15 !== void 0) {
                        let _tmp16: (_.KeyedDictionary<_.Value<_embedded>, Pattern<_embedded>, _embedded>) | undefined;
                        _tmp16 = void 0;
                        if (_.Dictionary.isDictionary<_embedded>(v[1])) {
                            _tmp16 = new _.KeyedDictionary();
                            for (const [_tmp18, _tmp17] of v[1]) {
                                let _tmp19: (_.Value<_embedded>) | undefined;
                                _tmp19 = _tmp18;
                                if (_tmp19 !== void 0) {
                                    let _tmp20: (Pattern<_embedded>) | undefined;
                                    _tmp20 = toPattern(_tmp17);
                                    if (_tmp20 !== void 0) {_tmp16.set(_tmp19, _tmp20); continue;};
                                };
                                _tmp16 = void 0;
                                break;
                            };
                        };
                        if (_tmp16 !== void 0) {result = {"_variant": "dict", "ctor": _tmp15, "members": _tmp16};};
                    };
                };
            };
        };
    };
    return result;
}

export function fromDCompound<_embedded = _i_EntityRef.Cap>(_v: DCompound<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "rec": {
            return _.Record(
                $compound,
                [
                    fromCRec<_embedded>(_v["ctor"]),
                    new _.Dictionary<_embedded>(
                        _.Array.from(_v["members"].entries()).map(([k, v]) => [k, fromPattern<_embedded>(v)])
                    )
                ]
            );
        };
        case "arr": {
            return _.Record(
                $compound,
                [
                    fromCArr<_embedded>(_v["ctor"]),
                    new _.Dictionary<_embedded>(
                        _.Array.from(_v["members"].entries()).map(([k, v]) => [k, fromPattern<_embedded>(v)])
                    )
                ]
            );
        };
        case "dict": {
            return _.Record(
                $compound,
                [
                    fromCDict<_embedded>(_v["ctor"]),
                    new _.Dictionary<_embedded>(
                        _.Array.from(_v["members"].entries()).map(([k, v]) => [k, fromPattern<_embedded>(v)])
                    )
                ]
            );
        };
    };
}

export function asCRec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CRec<_embedded> {
    let result = toCRec(v);
    if (result === void 0) throw new TypeError(`Invalid CRec: ${_.stringify(v)}`);
    return result;
}

export function toCRec<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CRec<_embedded> {
    let result: undefined | CRec<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $rec) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (number) | undefined;
                _tmp2 = typeof v[1] === 'number' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"label": _tmp1, "arity": _tmp2};};
            };
        };
    };
    return result;
}

export function fromCRec<_embedded = _i_EntityRef.Cap>(_v: CRec<_embedded>): _.Value<_embedded> {return _.Record($rec, [_v["label"], _v["arity"]]);}

export function asCArr<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CArr {
    let result = toCArr(v);
    if (result === void 0) throw new TypeError(`Invalid CArr: ${_.stringify(v)}`);
    return result;
}

export function toCArr<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CArr {
    let result: undefined | CArr;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $arr) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"arity": _tmp1};};
        };
    };
    return result;
}

export function fromCArr<_embedded = _i_EntityRef.Cap>(_v: CArr): _.Value<_embedded> {return _.Record($arr, [_v["arity"]]);}

export function asCDict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CDict {
    let result = toCDict(v);
    if (result === void 0) throw new TypeError(`Invalid CDict: ${_.stringify(v)}`);
    return result;
}

export function toCDict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CDict {
    let result: undefined | CDict;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $dict) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromCDict<_embedded = _i_EntityRef.Cap>(_v: CDict): _.Value<_embedded> {return _.Record($dict, []);}

