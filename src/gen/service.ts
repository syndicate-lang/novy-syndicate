import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $complete = Symbol.for("complete");
export const $failed = Symbol.for("failed");
export const $ready = Symbol.for("ready");
export const $started = Symbol.for("started");
export const __lit0 = Symbol.for("require-service");
export const __lit1 = Symbol.for("run-service");
export const __lit2 = Symbol.for("service-state");
export const __lit3 = Symbol.for("service-object");
export const __lit8 = Symbol.for("depends-on");
export const __lit9 = Symbol.for("core-service");

export type _embedded = _i_EntityRef.Cap;

export type RequireService<_embedded = _i_EntityRef.Cap> = {"serviceName": _.Value<_embedded>};

export type RunService<_embedded = _i_EntityRef.Cap> = {"serviceName": _.Value<_embedded>};

export type ServiceState<_embedded = _i_EntityRef.Cap> = {"serviceName": _.Value<_embedded>, "state": State};

export type ServiceObject<_embedded = _i_EntityRef.Cap> = {"serviceName": _.Value<_embedded>, "object": _.Value<_embedded>};

export type State = (
    {"_variant": "started"} |
    {"_variant": "ready"} |
    {"_variant": "failed"} |
    {"_variant": "complete"}
);

export type ServiceDependency<_embedded = _i_EntityRef.Cap> = {"depender": _.Value<_embedded>, "dependee": ServiceState<_embedded>};

export type CoreService<_embedded = _i_EntityRef.Cap> = {"serviceName": _.Value<_embedded>};


export function RequireService<_embedded = _i_EntityRef.Cap>(serviceName: _.Value<_embedded>): RequireService<_embedded> {return {"serviceName": serviceName};}

export function RunService<_embedded = _i_EntityRef.Cap>(serviceName: _.Value<_embedded>): RunService<_embedded> {return {"serviceName": serviceName};}

export function ServiceState<_embedded = _i_EntityRef.Cap>({serviceName, state}: {serviceName: _.Value<_embedded>, state: State}): ServiceState<_embedded> {return {"serviceName": serviceName, "state": state};}

export function ServiceObject<_embedded = _i_EntityRef.Cap>(
    {serviceName, object}: {serviceName: _.Value<_embedded>, object: _.Value<_embedded>}
): ServiceObject<_embedded> {return {"serviceName": serviceName, "object": object};}

export namespace State {
    export function started(): State {return {"_variant": "started"};};
    export function ready(): State {return {"_variant": "ready"};};
    export function failed(): State {return {"_variant": "failed"};};
    export function complete(): State {return {"_variant": "complete"};};
}

export function ServiceDependency<_embedded = _i_EntityRef.Cap>(
    {depender, dependee}: {depender: _.Value<_embedded>, dependee: ServiceState<_embedded>}
): ServiceDependency<_embedded> {return {"depender": depender, "dependee": dependee};}

export function CoreService<_embedded = _i_EntityRef.Cap>(serviceName: _.Value<_embedded>): CoreService<_embedded> {return {"serviceName": serviceName};}

export function asRequireService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): RequireService<_embedded> {
    let result = toRequireService(v);
    if (result === void 0) throw new TypeError(`Invalid RequireService: ${_.stringify(v)}`);
    return result;
}

export function toRequireService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | RequireService<_embedded> {
    let result: undefined | RequireService<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"serviceName": _tmp1};};
        };
    };
    return result;
}

export function fromRequireService<_embedded = _i_EntityRef.Cap>(_v: RequireService<_embedded>): _.Value<_embedded> {return _.Record(__lit0, [_v["serviceName"]]);}

export function asRunService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): RunService<_embedded> {
    let result = toRunService(v);
    if (result === void 0) throw new TypeError(`Invalid RunService: ${_.stringify(v)}`);
    return result;
}

export function toRunService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | RunService<_embedded> {
    let result: undefined | RunService<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit1) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"serviceName": _tmp1};};
        };
    };
    return result;
}

export function fromRunService<_embedded = _i_EntityRef.Cap>(_v: RunService<_embedded>): _.Value<_embedded> {return _.Record(__lit1, [_v["serviceName"]]);}

export function asServiceState<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): ServiceState<_embedded> {
    let result = toServiceState(v);
    if (result === void 0) throw new TypeError(`Invalid ServiceState: ${_.stringify(v)}`);
    return result;
}

export function toServiceState<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | ServiceState<_embedded> {
    let result: undefined | ServiceState<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit2) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (State) | undefined;
                _tmp2 = toState(v[1]);
                if (_tmp2 !== void 0) {result = {"serviceName": _tmp1, "state": _tmp2};};
            };
        };
    };
    return result;
}

export function fromServiceState<_embedded = _i_EntityRef.Cap>(_v: ServiceState<_embedded>): _.Value<_embedded> {
    return _.Record(__lit2, [_v["serviceName"], fromState<_embedded>(_v["state"])]);
}

export function asServiceObject<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): ServiceObject<_embedded> {
    let result = toServiceObject(v);
    if (result === void 0) throw new TypeError(`Invalid ServiceObject: ${_.stringify(v)}`);
    return result;
}

export function toServiceObject<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | ServiceObject<_embedded> {
    let result: undefined | ServiceObject<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit3) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Value<_embedded>) | undefined;
                _tmp2 = v[1];
                if (_tmp2 !== void 0) {result = {"serviceName": _tmp1, "object": _tmp2};};
            };
        };
    };
    return result;
}

export function fromServiceObject<_embedded = _i_EntityRef.Cap>(_v: ServiceObject<_embedded>): _.Value<_embedded> {return _.Record(__lit3, [_v["serviceName"], _v["object"]]);}

export function asState<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): State {
    let result = toState(v);
    if (result === void 0) throw new TypeError(`Invalid State: ${_.stringify(v)}`);
    return result;
}

export function toState<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | State {
    let _tmp0: (null) | undefined;
    let result: undefined | State;
    _tmp0 = _.is(v, $started) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "started"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $ready) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "ready"};};
        if (result === void 0) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v, $failed) ? null : void 0;
            if (_tmp2 !== void 0) {result = {"_variant": "failed"};};
            if (result === void 0) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v, $complete) ? null : void 0;
                if (_tmp3 !== void 0) {result = {"_variant": "complete"};};
            };
        };
    };
    return result;
}

export function fromState<_embedded = _i_EntityRef.Cap>(_v: State): _.Value<_embedded> {
    switch (_v._variant) {
        case "started": {return $started;};
        case "ready": {return $ready;};
        case "failed": {return $failed;};
        case "complete": {return $complete;};
    };
}

export function asServiceDependency<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): ServiceDependency<_embedded> {
    let result = toServiceDependency(v);
    if (result === void 0) throw new TypeError(`Invalid ServiceDependency: ${_.stringify(v)}`);
    return result;
}

export function toServiceDependency<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | ServiceDependency<_embedded> {
    let result: undefined | ServiceDependency<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit8) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (ServiceState<_embedded>) | undefined;
                _tmp2 = toServiceState(v[1]);
                if (_tmp2 !== void 0) {result = {"depender": _tmp1, "dependee": _tmp2};};
            };
        };
    };
    return result;
}

export function fromServiceDependency<_embedded = _i_EntityRef.Cap>(_v: ServiceDependency<_embedded>): _.Value<_embedded> {
    return _.Record(__lit8, [_v["depender"], fromServiceState<_embedded>(_v["dependee"])]);
}

export function asCoreService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): CoreService<_embedded> {
    let result = toCoreService(v);
    if (result === void 0) throw new TypeError(`Invalid CoreService: ${_.stringify(v)}`);
    return result;
}

export function toCoreService<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | CoreService<_embedded> {
    let result: undefined | CoreService<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit9) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {result = {"serviceName": _tmp1};};
        };
    };
    return result;
}

export function fromCoreService<_embedded = _i_EntityRef.Cap>(_v: CoreService<_embedded>): _.Value<_embedded> {return _.Record(__lit9, [_v["serviceName"]]);}

