import * as _ from "@preserves/core";

export const __lit0 = Symbol.for("racket-event");

export type RacketEvent<_embedded = _.GenericEmbedded> = {"source": _embedded, "event": _embedded};


export function RacketEvent<_embedded = _.GenericEmbedded>({source, event}: {source: _embedded, event: _embedded}): RacketEvent<_embedded> {return {"source": source, "event": event};}

export function asRacketEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): RacketEvent<_embedded> {
    let result = toRacketEvent(v);
    if (result === void 0) throw new TypeError(`Invalid RacketEvent: ${_.stringify(v)}`);
    return result;
}

export function toRacketEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | RacketEvent<_embedded> {
    let result: undefined | RacketEvent<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, __lit0) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {result = {"source": _tmp1, "event": _tmp2};};
            };
        };
    };
    return result;
}

export function fromRacketEvent<_embedded = _.GenericEmbedded>(_v: RacketEvent<_embedded>): _.Value<_embedded> {return _.Record(__lit0, [_.embed(_v["source"]), _.embed(_v["event"])]);}

