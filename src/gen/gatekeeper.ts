import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";
import * as _i_sturdy from "./sturdy";

export const $bind = Symbol.for("bind");
export const $resolve = Symbol.for("resolve");

export type _embedded = _i_EntityRef.Cap;

export type Resolve<_embedded = _i_EntityRef.Cap> = {"sturdyref": _i_sturdy.SturdyRef<_embedded>, "observer": _embedded};

export type Bind<_embedded = _i_EntityRef.Cap> = {"oid": _.Value<_embedded>, "key": _.Bytes, "target": _embedded};


export function Resolve<_embedded = _i_EntityRef.Cap>(
    {sturdyref, observer}: {sturdyref: _i_sturdy.SturdyRef<_embedded>, observer: _embedded}
): Resolve<_embedded> {return {"sturdyref": sturdyref, "observer": observer};}

export function Bind<_embedded = _i_EntityRef.Cap>({oid, key, target}: {oid: _.Value<_embedded>, key: _.Bytes, target: _embedded}): Bind<_embedded> {return {"oid": oid, "key": key, "target": target};}

export function asResolve<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Resolve<_embedded> {
    let result = toResolve(v);
    if (result === void 0) throw new TypeError(`Invalid Resolve: ${_.stringify(v)}`);
    return result;
}

export function toResolve<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Resolve<_embedded> {
    let result: undefined | Resolve<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $resolve) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_i_sturdy.SturdyRef<_embedded>) | undefined;
            _tmp1 = _i_sturdy.toSturdyRef(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {result = {"sturdyref": _tmp1, "observer": _tmp2};};
            };
        };
    };
    return result;
}

export function fromResolve<_embedded = _i_EntityRef.Cap>(_v: Resolve<_embedded>): _.Value<_embedded> {
    return _.Record(
        $resolve,
        [_i_sturdy.fromSturdyRef<_embedded>(_v["sturdyref"]), _.embed(_v["observer"])]
    );
}

export function asBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Bind<_embedded> {
    let result = toBind(v);
    if (result === void 0) throw new TypeError(`Invalid Bind: ${_.stringify(v)}`);
    return result;
}

export function toBind<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Bind<_embedded> {
    let result: undefined | Bind<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $bind) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_.Value<_embedded>) | undefined;
            _tmp1 = v[0];
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Bytes) | undefined;
                _tmp2 = _.Bytes.isBytes(v[1]) ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (_embedded) | undefined;
                    _tmp3 = _.isEmbedded<_embedded>(v[2]) ? v[2].embeddedValue : void 0;
                    if (_tmp3 !== void 0) {result = {"oid": _tmp1, "key": _tmp2, "target": _tmp3};};
                };
            };
        };
    };
    return result;
}

export function fromBind<_embedded = _i_EntityRef.Cap>(_v: Bind<_embedded>): _.Value<_embedded> {return _.Record($bind, [_v["oid"], _v["key"], _.embed(_v["target"])]);}

