import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $Observe = Symbol.for("Observe");
export const $claimNick = Symbol.for("claimNick");
export const $joinedUser = Symbol.for("joinedUser");
export const $nickConflict = Symbol.for("nickConflict");
export const $says = Symbol.for("says");
export const $user = Symbol.for("user");
export const __lit5 = true;

export type _embedded = _i_EntityRef.Cap;

export type UserId = number;

export type Join<_embedded = _i_EntityRef.Cap> = {"uid": UserId, "handle": _embedded};

export type Session<_embedded = _i_EntityRef.Cap> = (
    {"_variant": "observeUsers", "observer": _embedded} |
    {"_variant": "observeSpeech", "observer": _embedded} |
    {"_variant": "NickClaim", "value": NickClaim<_embedded>} |
    {"_variant": "Says", "value": Says}
);

export type NickClaim<_embedded = _i_EntityRef.Cap> = {"uid": UserId, "name": string, "k": _embedded};

export type NickClaimResponse = ({"_variant": "true"} | {"_variant": "NickConflict", "value": NickConflict});

export type UserInfo = {"uid": UserId, "name": string};

export type Says = {"who": UserId, "what": string};

export type NickConflict = null;


export function UserId(value: number): UserId {return value;}

export function Join<_embedded = _i_EntityRef.Cap>({uid, handle}: {uid: UserId, handle: _embedded}): Join<_embedded> {return {"uid": uid, "handle": handle};}

export namespace Session {
    export function observeUsers<_embedded = _i_EntityRef.Cap>(observer: _embedded): Session<_embedded> {return {"_variant": "observeUsers", "observer": observer};};
    export function observeSpeech<_embedded = _i_EntityRef.Cap>(observer: _embedded): Session<_embedded> {return {"_variant": "observeSpeech", "observer": observer};};
    export function NickClaim<_embedded = _i_EntityRef.Cap>(value: NickClaim<_embedded>): Session<_embedded> {return {"_variant": "NickClaim", "value": value};};
    export function Says<_embedded = _i_EntityRef.Cap>(value: Says): Session<_embedded> {return {"_variant": "Says", "value": value};};
}

export function NickClaim<_embedded = _i_EntityRef.Cap>({uid, name, k}: {uid: UserId, name: string, k: _embedded}): NickClaim<_embedded> {return {"uid": uid, "name": name, "k": k};}

export namespace NickClaimResponse {
    export function $true(): NickClaimResponse {return {"_variant": "true"};};
    export function NickConflict(value: NickConflict): NickClaimResponse {return {"_variant": "NickConflict", "value": value};};
}

export function UserInfo({uid, name}: {uid: UserId, name: string}): UserInfo {return {"uid": uid, "name": name};}

export function Says({who, what}: {who: UserId, what: string}): Says {return {"who": who, "what": what};}

export function NickConflict(): NickConflict {return null;}

export function asUserId<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): UserId {
    let result = toUserId(v);
    if (result === void 0) throw new TypeError(`Invalid UserId: ${_.stringify(v)}`);
    return result;
}

export function toUserId<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | UserId {
    let _tmp0: (number) | undefined;
    let result: undefined | UserId;
    _tmp0 = typeof v === 'number' ? v : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromUserId<_embedded = _i_EntityRef.Cap>(_v: UserId): _.Value<_embedded> {return _v;}

export function asJoin<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Join<_embedded> {
    let result = toJoin(v);
    if (result === void 0) throw new TypeError(`Invalid Join: ${_.stringify(v)}`);
    return result;
}

export function toJoin<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Join<_embedded> {
    let result: undefined | Join<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $joinedUser) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (UserId) | undefined;
            _tmp1 = toUserId(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {result = {"uid": _tmp1, "handle": _tmp2};};
            };
        };
    };
    return result;
}

export function fromJoin<_embedded = _i_EntityRef.Cap>(_v: Join<_embedded>): _.Value<_embedded> {
    return _.Record($joinedUser, [fromUserId<_embedded>(_v["uid"]), _.embed(_v["handle"])]);
}

export function asSession<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Session<_embedded> {
    let result = toSession(v);
    if (result === void 0) throw new TypeError(`Invalid Session: ${_.stringify(v)}`);
    return result;
}

export function toSession<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Session<_embedded> {
    let result: undefined | Session<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $Observe) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (null) | undefined;
            _tmp1 = _.is(v[0], $user) ? null : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {result = {"_variant": "observeUsers", "observer": _tmp2};};
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp3: (null) | undefined;
            _tmp3 = _.is(v.label, $Observe) ? null : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (null) | undefined;
                _tmp4 = _.is(v[0], $says) ? null : void 0;
                if (_tmp4 !== void 0) {
                    let _tmp5: (_embedded) | undefined;
                    _tmp5 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                    if (_tmp5 !== void 0) {result = {"_variant": "observeSpeech", "observer": _tmp5};};
                };
            };
        };
        if (result === void 0) {
            let _tmp6: (NickClaim<_embedded>) | undefined;
            _tmp6 = toNickClaim(v);
            if (_tmp6 !== void 0) {result = {"_variant": "NickClaim", "value": _tmp6};};
            if (result === void 0) {
                let _tmp7: (Says) | undefined;
                _tmp7 = toSays(v);
                if (_tmp7 !== void 0) {result = {"_variant": "Says", "value": _tmp7};};
            };
        };
    };
    return result;
}

export function fromSession<_embedded = _i_EntityRef.Cap>(_v: Session<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "observeUsers": {return _.Record($Observe, [$user, _.embed(_v["observer"])]);};
        case "observeSpeech": {return _.Record($Observe, [$says, _.embed(_v["observer"])]);};
        case "NickClaim": {return fromNickClaim<_embedded>(_v.value);};
        case "Says": {return fromSays<_embedded>(_v.value);};
    };
}

export function asNickClaim<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): NickClaim<_embedded> {
    let result = toNickClaim(v);
    if (result === void 0) throw new TypeError(`Invalid NickClaim: ${_.stringify(v)}`);
    return result;
}

export function toNickClaim<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | NickClaim<_embedded> {
    let result: undefined | NickClaim<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $claimNick) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (UserId) | undefined;
            _tmp1 = toUserId(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {
                    let _tmp3: (_embedded) | undefined;
                    _tmp3 = _.isEmbedded<_embedded>(v[2]) ? v[2].embeddedValue : void 0;
                    if (_tmp3 !== void 0) {result = {"uid": _tmp1, "name": _tmp2, "k": _tmp3};};
                };
            };
        };
    };
    return result;
}

export function fromNickClaim<_embedded = _i_EntityRef.Cap>(_v: NickClaim<_embedded>): _.Value<_embedded> {
    return _.Record($claimNick, [fromUserId<_embedded>(_v["uid"]), _v["name"], _.embed(_v["k"])]);
}

export function asNickClaimResponse<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): NickClaimResponse {
    let result = toNickClaimResponse(v);
    if (result === void 0) throw new TypeError(`Invalid NickClaimResponse: ${_.stringify(v)}`);
    return result;
}

export function toNickClaimResponse<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | NickClaimResponse {
    let _tmp0: (null) | undefined;
    let result: undefined | NickClaimResponse;
    _tmp0 = _.is(v, __lit5) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "true"};};
    if (result === void 0) {
        let _tmp1: (NickConflict) | undefined;
        _tmp1 = toNickConflict(v);
        if (_tmp1 !== void 0) {result = {"_variant": "NickConflict", "value": _tmp1};};
    };
    return result;
}

export function fromNickClaimResponse<_embedded = _i_EntityRef.Cap>(_v: NickClaimResponse): _.Value<_embedded> {
    switch (_v._variant) {
        case "true": {return __lit5;};
        case "NickConflict": {return fromNickConflict<_embedded>(_v.value);};
    };
}

export function asUserInfo<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): UserInfo {
    let result = toUserInfo(v);
    if (result === void 0) throw new TypeError(`Invalid UserInfo: ${_.stringify(v)}`);
    return result;
}

export function toUserInfo<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | UserInfo {
    let result: undefined | UserInfo;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $user) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (UserId) | undefined;
            _tmp1 = toUserId(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"uid": _tmp1, "name": _tmp2};};
            };
        };
    };
    return result;
}

export function fromUserInfo<_embedded = _i_EntityRef.Cap>(_v: UserInfo): _.Value<_embedded> {return _.Record($user, [fromUserId<_embedded>(_v["uid"]), _v["name"]]);}

export function asSays<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Says {
    let result = toSays(v);
    if (result === void 0) throw new TypeError(`Invalid Says: ${_.stringify(v)}`);
    return result;
}

export function toSays<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Says {
    let result: undefined | Says;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $says) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (UserId) | undefined;
            _tmp1 = toUserId(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (string) | undefined;
                _tmp2 = typeof v[1] === 'string' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"who": _tmp1, "what": _tmp2};};
            };
        };
    };
    return result;
}

export function fromSays<_embedded = _i_EntityRef.Cap>(_v: Says): _.Value<_embedded> {return _.Record($says, [fromUserId<_embedded>(_v["who"]), _v["what"]]);}

export function asNickConflict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): NickConflict {
    let result = toNickConflict(v);
    if (result === void 0) throw new TypeError(`Invalid NickConflict: ${_.stringify(v)}`);
    return result;
}

export function toNickConflict<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | NickConflict {
    let result: undefined | NickConflict;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $nickConflict) ? null : void 0;
        if (_tmp0 !== void 0) {result = null;};
    };
    return result;
}

export function fromNickConflict<_embedded = _i_EntityRef.Cap>(_v: NickConflict): _.Value<_embedded> {return _.Record($nickConflict, []);}

