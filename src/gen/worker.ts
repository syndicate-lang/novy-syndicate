import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";

export const $Instance = Symbol.for("Instance");

export type _embedded = _i_EntityRef.Cap;

export type Instance<_embedded = _i_EntityRef.Cap> = {"name": string, "argument": _.Value<_embedded>};


export function Instance<_embedded = _i_EntityRef.Cap>({name, argument}: {name: string, argument: _.Value<_embedded>}): Instance<_embedded> {return {"name": name, "argument": argument};}

export function asInstance<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Instance<_embedded> {
    let result = toInstance(v);
    if (result === void 0) throw new TypeError(`Invalid Instance: ${_.stringify(v)}`);
    return result;
}

export function toInstance<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Instance<_embedded> {
    let result: undefined | Instance<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $Instance) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Value<_embedded>) | undefined;
                _tmp2 = v[1];
                if (_tmp2 !== void 0) {result = {"name": _tmp1, "argument": _tmp2};};
            };
        };
    };
    return result;
}

export function fromInstance<_embedded = _i_EntityRef.Cap>(_v: Instance<_embedded>): _.Value<_embedded> {return _.Record($Instance, [_v["name"], _v["argument"]]);}

