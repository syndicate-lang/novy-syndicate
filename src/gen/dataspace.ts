import * as _ from "@preserves/core";
import * as _i_EntityRef from "../runtime/actor";
import * as _i_dataspacePatterns from "./dataspacePatterns";

export const $Observe = Symbol.for("Observe");

export type _embedded = _i_EntityRef.Cap;

export type Observe<_embedded = _i_EntityRef.Cap> = {"pattern": _i_dataspacePatterns.Pattern<_embedded>, "observer": _embedded};


export function Observe<_embedded = _i_EntityRef.Cap>(
    {pattern, observer}: {pattern: _i_dataspacePatterns.Pattern<_embedded>, observer: _embedded}
): Observe<_embedded> {return {"pattern": pattern, "observer": observer};}

export function asObserve<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): Observe<_embedded> {
    let result = toObserve(v);
    if (result === void 0) throw new TypeError(`Invalid Observe: ${_.stringify(v)}`);
    return result;
}

export function toObserve<_embedded = _i_EntityRef.Cap>(v: _.Value<_embedded>): undefined | Observe<_embedded> {
    let result: undefined | Observe<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $Observe) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_i_dataspacePatterns.Pattern<_embedded>) | undefined;
            _tmp1 = _i_dataspacePatterns.toPattern(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (_embedded) | undefined;
                _tmp2 = _.isEmbedded<_embedded>(v[1]) ? v[1].embeddedValue : void 0;
                if (_tmp2 !== void 0) {result = {"pattern": _tmp1, "observer": _tmp2};};
            };
        };
    };
    return result;
}

export function fromObserve<_embedded = _i_EntityRef.Cap>(_v: Observe<_embedded>): _.Value<_embedded> {
    return _.Record(
        $Observe,
        [
            _i_dataspacePatterns.fromPattern<_embedded>(_v["pattern"]),
            _.embed(_v["observer"])
        ]
    );
}

