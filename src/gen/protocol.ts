import * as _ from "@preserves/core";

export const $assert = Symbol.for("assert");
export const $error = Symbol.for("error");
export const $message = Symbol.for("message");
export const $retract = Symbol.for("retract");
export const $sync = Symbol.for("sync");

export type Packet<_embedded = _.GenericEmbedded> = (
    {"_variant": "Turn", "value": Turn<_embedded>} |
    {"_variant": "Error", "value": Error<_embedded>}
);

export type Error<_embedded = _.GenericEmbedded> = {"message": string, "detail": _.Value<_embedded>};

export type Assertion<_embedded = _.GenericEmbedded> = _.Value<_embedded>;

export type Handle = number;

export type Event<_embedded = _.GenericEmbedded> = (
    {"_variant": "Assert", "value": Assert<_embedded>} |
    {"_variant": "Retract", "value": Retract} |
    {"_variant": "Message", "value": Message<_embedded>} |
    {"_variant": "Sync", "value": Sync<_embedded>}
);

export type Oid = number;

export type Turn<_embedded = _.GenericEmbedded> = Array<TurnEvent<_embedded>>;

export type TurnEvent<_embedded = _.GenericEmbedded> = {"oid": Oid, "event": Event<_embedded>};

export type Assert<_embedded = _.GenericEmbedded> = {"assertion": Assertion<_embedded>, "handle": Handle};

export type Retract = {"handle": Handle};

export type Message<_embedded = _.GenericEmbedded> = {"body": Assertion<_embedded>};

export type Sync<_embedded = _.GenericEmbedded> = {"peer": _embedded};


export namespace Packet {
    export function Turn<_embedded = _.GenericEmbedded>(value: Turn<_embedded>): Packet<_embedded> {return {"_variant": "Turn", "value": value};};
    export function Error<_embedded = _.GenericEmbedded>(value: Error<_embedded>): Packet<_embedded> {return {"_variant": "Error", "value": value};};
}

export function Error<_embedded = _.GenericEmbedded>({message, detail}: {message: string, detail: _.Value<_embedded>}): Error<_embedded> {return {"message": message, "detail": detail};}

export function Assertion<_embedded = _.GenericEmbedded>(value: _.Value<_embedded>): Assertion<_embedded> {return value;}

export function Handle(value: number): Handle {return value;}

export namespace Event {
    export function Assert<_embedded = _.GenericEmbedded>(value: Assert<_embedded>): Event<_embedded> {return {"_variant": "Assert", "value": value};};
    export function Retract<_embedded = _.GenericEmbedded>(value: Retract): Event<_embedded> {return {"_variant": "Retract", "value": value};};
    export function Message<_embedded = _.GenericEmbedded>(value: Message<_embedded>): Event<_embedded> {return {"_variant": "Message", "value": value};};
    export function Sync<_embedded = _.GenericEmbedded>(value: Sync<_embedded>): Event<_embedded> {return {"_variant": "Sync", "value": value};};
}

export function Oid(value: number): Oid {return value;}

export function Turn<_embedded = _.GenericEmbedded>(value: Array<TurnEvent<_embedded>>): Turn<_embedded> {return value;}

export function TurnEvent<_embedded = _.GenericEmbedded>({oid, event}: {oid: Oid, event: Event<_embedded>}): TurnEvent<_embedded> {return {"oid": oid, "event": event};}

export function Assert<_embedded = _.GenericEmbedded>({assertion, handle}: {assertion: Assertion<_embedded>, handle: Handle}): Assert<_embedded> {return {"assertion": assertion, "handle": handle};}

export function Retract(handle: Handle): Retract {return {"handle": handle};}

export function Message<_embedded = _.GenericEmbedded>(body: Assertion<_embedded>): Message<_embedded> {return {"body": body};}

export function Sync<_embedded = _.GenericEmbedded>(peer: _embedded): Sync<_embedded> {return {"peer": peer};}

export function asPacket<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Packet<_embedded> {
    let result = toPacket(v);
    if (result === void 0) throw new TypeError(`Invalid Packet: ${_.stringify(v)}`);
    return result;
}

export function toPacket<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Packet<_embedded> {
    let _tmp0: (Turn<_embedded>) | undefined;
    let result: undefined | Packet<_embedded>;
    _tmp0 = toTurn(v);
    if (_tmp0 !== void 0) {result = {"_variant": "Turn", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (Error<_embedded>) | undefined;
        _tmp1 = toError(v);
        if (_tmp1 !== void 0) {result = {"_variant": "Error", "value": _tmp1};};
    };
    return result;
}

export function fromPacket<_embedded = _.GenericEmbedded>(_v: Packet<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "Turn": {return fromTurn<_embedded>(_v.value);};
        case "Error": {return fromError<_embedded>(_v.value);};
    };
}

export function asError<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Error<_embedded> {
    let result = toError(v);
    if (result === void 0) throw new TypeError(`Invalid Error: ${_.stringify(v)}`);
    return result;
}

export function toError<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Error<_embedded> {
    let result: undefined | Error<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $error) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (string) | undefined;
            _tmp1 = typeof v[0] === 'string' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (_.Value<_embedded>) | undefined;
                _tmp2 = v[1];
                if (_tmp2 !== void 0) {result = {"message": _tmp1, "detail": _tmp2};};
            };
        };
    };
    return result;
}

export function fromError<_embedded = _.GenericEmbedded>(_v: Error<_embedded>): _.Value<_embedded> {return _.Record($error, [_v["message"], _v["detail"]]);}

export function asAssertion<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Assertion<_embedded> {
    let result = toAssertion(v);
    if (result === void 0) throw new TypeError(`Invalid Assertion: ${_.stringify(v)}`);
    return result;
}

export function toAssertion<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Assertion<_embedded> {
    let _tmp0: (_.Value<_embedded>) | undefined;
    let result: undefined | Assertion<_embedded>;
    _tmp0 = v;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromAssertion<_embedded = _.GenericEmbedded>(_v: Assertion<_embedded>): _.Value<_embedded> {return _v;}

export function asHandle<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Handle {
    let result = toHandle(v);
    if (result === void 0) throw new TypeError(`Invalid Handle: ${_.stringify(v)}`);
    return result;
}

export function toHandle<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Handle {
    let _tmp0: (number) | undefined;
    let result: undefined | Handle;
    _tmp0 = typeof v === 'number' ? v : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromHandle<_embedded = _.GenericEmbedded>(_v: Handle): _.Value<_embedded> {return _v;}

export function asEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Event<_embedded> {
    let result = toEvent(v);
    if (result === void 0) throw new TypeError(`Invalid Event: ${_.stringify(v)}`);
    return result;
}

export function toEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Event<_embedded> {
    let _tmp0: (Assert<_embedded>) | undefined;
    let result: undefined | Event<_embedded>;
    _tmp0 = toAssert(v);
    if (_tmp0 !== void 0) {result = {"_variant": "Assert", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (Retract) | undefined;
        _tmp1 = toRetract(v);
        if (_tmp1 !== void 0) {result = {"_variant": "Retract", "value": _tmp1};};
        if (result === void 0) {
            let _tmp2: (Message<_embedded>) | undefined;
            _tmp2 = toMessage(v);
            if (_tmp2 !== void 0) {result = {"_variant": "Message", "value": _tmp2};};
            if (result === void 0) {
                let _tmp3: (Sync<_embedded>) | undefined;
                _tmp3 = toSync(v);
                if (_tmp3 !== void 0) {result = {"_variant": "Sync", "value": _tmp3};};
            };
        };
    };
    return result;
}

export function fromEvent<_embedded = _.GenericEmbedded>(_v: Event<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "Assert": {return fromAssert<_embedded>(_v.value);};
        case "Retract": {return fromRetract<_embedded>(_v.value);};
        case "Message": {return fromMessage<_embedded>(_v.value);};
        case "Sync": {return fromSync<_embedded>(_v.value);};
    };
}

export function asOid<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Oid {
    let result = toOid(v);
    if (result === void 0) throw new TypeError(`Invalid Oid: ${_.stringify(v)}`);
    return result;
}

export function toOid<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Oid {
    let _tmp0: (number) | undefined;
    let result: undefined | Oid;
    _tmp0 = typeof v === 'number' ? v : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromOid<_embedded = _.GenericEmbedded>(_v: Oid): _.Value<_embedded> {return _v;}

export function asTurn<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Turn<_embedded> {
    let result = toTurn(v);
    if (result === void 0) throw new TypeError(`Invalid Turn: ${_.stringify(v)}`);
    return result;
}

export function toTurn<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Turn<_embedded> {
    let _tmp0: (Array<TurnEvent<_embedded>>) | undefined;
    let result: undefined | Turn<_embedded>;
    _tmp0 = void 0;
    if (_.Array.isArray(v)) {
        _tmp0 = [];
        for (const _tmp1 of v) {
            let _tmp2: (TurnEvent<_embedded>) | undefined;
            _tmp2 = toTurnEvent(_tmp1);
            if (_tmp2 !== void 0) {_tmp0.push(_tmp2); continue;};
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromTurn<_embedded = _.GenericEmbedded>(_v: Turn<_embedded>): _.Value<_embedded> {return _v.map(v => fromTurnEvent<_embedded>(v));}

export function asTurnEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): TurnEvent<_embedded> {
    let result = toTurnEvent(v);
    if (result === void 0) throw new TypeError(`Invalid TurnEvent: ${_.stringify(v)}`);
    return result;
}

export function toTurnEvent<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | TurnEvent<_embedded> {
    let result: undefined | TurnEvent<_embedded>;
    if (_.Array.isArray(v) && v.length === 2) {
        let _tmp0: (Oid) | undefined;
        _tmp0 = toOid(v[0]);
        if (_tmp0 !== void 0) {
            let _tmp1: (Event<_embedded>) | undefined;
            _tmp1 = toEvent(v[1]);
            if (_tmp1 !== void 0) {result = {"oid": _tmp0, "event": _tmp1};};
        };
    };
    return result;
}

export function fromTurnEvent<_embedded = _.GenericEmbedded>(_v: TurnEvent<_embedded>): _.Value<_embedded> {return [fromOid<_embedded>(_v["oid"]), fromEvent<_embedded>(_v["event"])];}

export function asAssert<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Assert<_embedded> {
    let result = toAssert(v);
    if (result === void 0) throw new TypeError(`Invalid Assert: ${_.stringify(v)}`);
    return result;
}

export function toAssert<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Assert<_embedded> {
    let result: undefined | Assert<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $assert) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Assertion<_embedded>) | undefined;
            _tmp1 = toAssertion(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (Handle) | undefined;
                _tmp2 = toHandle(v[1]);
                if (_tmp2 !== void 0) {result = {"assertion": _tmp1, "handle": _tmp2};};
            };
        };
    };
    return result;
}

export function fromAssert<_embedded = _.GenericEmbedded>(_v: Assert<_embedded>): _.Value<_embedded> {
    return _.Record(
        $assert,
        [fromAssertion<_embedded>(_v["assertion"]), fromHandle<_embedded>(_v["handle"])]
    );
}

export function asRetract<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Retract {
    let result = toRetract(v);
    if (result === void 0) throw new TypeError(`Invalid Retract: ${_.stringify(v)}`);
    return result;
}

export function toRetract<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Retract {
    let result: undefined | Retract;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $retract) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Handle) | undefined;
            _tmp1 = toHandle(v[0]);
            if (_tmp1 !== void 0) {result = {"handle": _tmp1};};
        };
    };
    return result;
}

export function fromRetract<_embedded = _.GenericEmbedded>(_v: Retract): _.Value<_embedded> {return _.Record($retract, [fromHandle<_embedded>(_v["handle"])]);}

export function asMessage<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Message<_embedded> {
    let result = toMessage(v);
    if (result === void 0) throw new TypeError(`Invalid Message: ${_.stringify(v)}`);
    return result;
}

export function toMessage<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Message<_embedded> {
    let result: undefined | Message<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $message) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Assertion<_embedded>) | undefined;
            _tmp1 = toAssertion(v[0]);
            if (_tmp1 !== void 0) {result = {"body": _tmp1};};
        };
    };
    return result;
}

export function fromMessage<_embedded = _.GenericEmbedded>(_v: Message<_embedded>): _.Value<_embedded> {return _.Record($message, [fromAssertion<_embedded>(_v["body"])]);}

export function asSync<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Sync<_embedded> {
    let result = toSync(v);
    if (result === void 0) throw new TypeError(`Invalid Sync: ${_.stringify(v)}`);
    return result;
}

export function toSync<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Sync<_embedded> {
    let result: undefined | Sync<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $sync) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (_embedded) | undefined;
            _tmp1 = _.isEmbedded<_embedded>(v[0]) ? v[0].embeddedValue : void 0;
            if (_tmp1 !== void 0) {result = {"peer": _tmp1};};
        };
    };
    return result;
}

export function fromSync<_embedded = _.GenericEmbedded>(_v: Sync<_embedded>): _.Value<_embedded> {return _.Record($sync, [_.embed(_v["peer"])]);}

