import * as _ from "@preserves/core";
import * as _i_EntityRef from "../../runtime/actor";

export const $BoxState = Symbol.for("BoxState");
export const $SetBox = Symbol.for("SetBox");

export type _embedded = _i_EntityRef.Ref;

export type BoxState = {"value": number};

export type SetBox = {"value": number};


export function BoxState(value: number): BoxState {return {"value": value};}

export function SetBox(value: number): SetBox {return {"value": value};}

export function asBoxState<_embedded = _i_EntityRef.Ref>(v: _.Value<_embedded>): BoxState {
    let result = toBoxState(v);
    if (result === void 0) throw new TypeError(`Invalid BoxState: ${_.stringify(v)}`);
    return result;
}

export function toBoxState<_embedded = _i_EntityRef.Ref>(v: _.Value<_embedded>): undefined | BoxState {
    let result: undefined | BoxState;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $BoxState) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"value": _tmp1};};
        };
    };
    return result;
}

export function fromBoxState<_embedded = _i_EntityRef.Ref>(_v: BoxState): _.Value<_embedded> {return _.Record($BoxState, [_v["value"]]);}

export function asSetBox<_embedded = _i_EntityRef.Ref>(v: _.Value<_embedded>): SetBox {
    let result = toSetBox(v);
    if (result === void 0) throw new TypeError(`Invalid SetBox: ${_.stringify(v)}`);
    return result;
}

export function toSetBox<_embedded = _i_EntityRef.Ref>(v: _.Value<_embedded>): undefined | SetBox {
    let result: undefined | SetBox;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $SetBox) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (number) | undefined;
            _tmp1 = typeof v[0] === 'number' ? v[0] : void 0;
            if (_tmp1 !== void 0) {result = {"value": _tmp1};};
        };
    };
    return result;
}

export function fromSetBox<_embedded = _i_EntityRef.Ref>(_v: SetBox): _.Value<_embedded> {return _.Record($SetBox, [_v["value"]]);}

