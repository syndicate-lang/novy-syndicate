import { Bytes, underlying } from '@preserves/core';
import * as node_crypto from 'crypto';

export const KEY_LENGTH = 16; // 128 bits

export const newKey: () => Promise<Bytes> =
    (typeof crypto !== 'undefined' && 'getRandomValues' in crypto)
    ? (async () => {
        const bs = new Bytes(KEY_LENGTH);
        crypto.getRandomValues(underlying(bs));
        return bs;
    })
    : (async () => Bytes.from(node_crypto.randomBytes(KEY_LENGTH)));

export const mac: (secretKey: Bytes, data: Bytes) => Promise<Bytes> =
    (typeof crypto !== 'undefined' && 'subtle' in crypto)
    ? (async (secretKey, data) => {
        if (secretKey.length !== KEY_LENGTH) throw new Error("Invalid key length");
        const k = await window.crypto.subtle.importKey(
            "raw",
            underlying(secretKey),
            {
                hash: 'SHA-256',
                name: 'HMAC',
            },
            false,
            ['sign']);
        const bs = await window.crypto.subtle.sign({ name: 'HMAC' }, k, underlying(data));
        return Bytes.from(new Uint8Array(bs, 0, KEY_LENGTH));
    })
    : (typeof node_crypto.createHmac !== 'undefined')
    ? (async (secretKey, data) => {
        const hmac = node_crypto.createHmac('sha256', underlying(secretKey));
        hmac.update(underlying(data));
        return Bytes.from(hmac.digest().subarray(0, KEY_LENGTH));
    })
    : (async (_secretKey, _data) => {
        throw new Error('No HMAC SHA-256 available');
    });
