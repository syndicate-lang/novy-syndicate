import * as S from '../gen/sturdy.js';
import { Oid } from '../gen/protocol.js';
import { Ref } from '../runtime/actor.js';
import { Decoder, DecoderState, Encoder, EncoderState, GenericEmbedded, neverEmbeddedType, EmbeddedType, Value } from '@preserves/core';

export type WireSymbol = { oid: Oid, ref: Ref, count: number };

export const wireRefEmbeddedType: EmbeddedType<S.WireRef> = {
    decode(s: DecoderState): S.WireRef {
        return S.asWireRef(new Decoder<any>(s).next());
    },

    encode(s: EncoderState, v: S.WireRef): void {
        new Encoder<any>(s, neverEmbeddedType).push(S.fromWireRef(v));
    },

    fromValue(v: Value<GenericEmbedded>): S.WireRef {
        return S.asWireRef(v as Value<S._embedded>);
    },

    toValue(v: S.WireRef): Value<GenericEmbedded> {
        return S.fromWireRef(v) as Value<GenericEmbedded>;
    }
};
