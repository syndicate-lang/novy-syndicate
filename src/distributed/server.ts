import { Actor, Handle, Ref, Turn } from '../runtime/actor.js';
import { Dataspace, during, observe, P } from '../runtime/dataspace.js';
import { Relay, spawnRelay } from '../transport/relay.js';
import * as net from 'net';

import { mint, sturdyEncode, validate } from '../transport/sturdy.js';
import { KEY_LENGTH } from '../transport/cryptography.js';
import { attenuate } from '../runtime/rewrite.js';
import { Bytes, embed, Embedded } from '@preserves/core';
import { Attenuation, fromSturdyRef } from '../gen/sturdy.js';
import { $bind, Bind, fromBind, toResolve } from '../gen/gatekeeper.js';

new Actor(t => {
    t.activeFacet.preventInertCheck();

    const ds = t.ref(new Dataspace());

    const dsOid = 'syndicate';
    const dsKey = new Bytes(KEY_LENGTH);
    t.assert(ds, fromBind(Bind({
        oid: dsOid,
        key: dsKey,
        target: ds,
    })));
    mint(dsOid, dsKey).then(v => {
        console.log(fromSturdyRef(v).asPreservesText());
        console.log(sturdyEncode(fromSturdyRef(v)).toHex());
    });

    function spawnConnection(t: Turn, socket: net.Socket) {
        console.log('connection', socket.remoteAddress, socket.remotePort);
        spawnRelay(t, {
            packetWriter: bs => socket.write(bs),
            setup(t: Turn, r: Relay) {
                socket.on('error', err => t.freshen(t =>
                    ((err as any).code === 'ECONNRESET') ? t.stopActor() : t.crash(err)));
                socket.on('close', () => t.freshen(t => t.stopActor()));
                socket.on('end', () => t.freshen(t => t.stopActor()));
                socket.on('data', data => r.accept(data));
                t.activeFacet.actor.atExit(() => socket.destroy());
            },
            initialRef: t.ref(during(async (t, a0) => {
                const a = toResolve(a0);
                if (a === void 0) return null;
                const r = a.sturdyref;
                let facet = t.facet(t => {
                    observe(t, ds, P.rec($bind, P.lit(r.oid), P.bind(), P.bind()),
                            during(async (t, bindings) => {
                                const [key, embeddedTarget] = bindings as [Bytes, Embedded<Ref>];
                                const target = embeddedTarget.embeddedValue;
                                if (!await validate(r, key)) return null;
                                const cavs: Attenuation = [];
                                r.caveatChain.forEach(cs => cavs.push(... cs));
                                const attenuated_ds = attenuate(target, ... cavs);
                                let replyHandle: Handle | undefined;
                                t.freshen(t => replyHandle = t.assert(a.observer, embed(attenuated_ds)));
                                return t => t.retract(replyHandle);
                            }));
                });
                return t => t.stop(facet);
            })),
            // debug: true,
        });
    }

    t.spawn(t => {
        const server = net.createServer(socket => t.freshen(t => spawnConnection(t, socket)));
        server.on('error', err => t.freshen(t => t.crash(err)));
        server.listen(5999, '0.0.0.0', 512);
        t.activeFacet.preventInertCheck();
        t.activeFacet.actor.atExit(() => {
            try {
                server.close();
            } catch (e) {
                console.error(e);
            }
        });
    });
});
