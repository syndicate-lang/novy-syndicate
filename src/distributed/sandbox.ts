import { Actor, Ref, Turn } from "../runtime/actor";
import { Relay, spawnRelay } from "../transport/relay";
import { sturdyDecode } from "../transport/sturdy";
import { asSturdyRef } from "../gen/sturdy";
import { Resolve, fromResolve } from "../gen/gatekeeper";
import { during } from "../runtime/dataspace";

import * as net from 'net';
import { Bytes } from "@preserves/core";

const [ moduleName, hexCap ] = process.argv.slice(2);
const cap = sturdyDecode(Bytes.fromHex(hexCap ?? ''));

const socket = net.createConnection({ port: 5999, host: 'localhost' }, () => {
    new Actor(t => {
        let shutdownRef: Ref;
        const reenable = t.activeFacet.preventInertCheck();
        const connectionClosedRef = t.ref({
            retract(t) { t.stopActor(); },
        });
        spawnRelay(t, {
            packetWriter: bs => socket.write(bs),
            setup(t: Turn, r: Relay) {
                socket.on('error', err => t.freshen(t =>
                    ((err as any).code === 'ECONNRESET') ? t.stopActor() : t.crash(err)));
                socket.on('close', () => t.freshen(t => t.stopActor()));
                socket.on('end', () => t.freshen(t => t.stopActor()));
                socket.on('data', data => r.accept(data));
                t.activeFacet.actor.atExit(() => socket.destroy());
                t.assert(connectionClosedRef, true);
                shutdownRef = t.ref({
                    retract(t) { t.stopActor(); }
                });
            },
            initialOid: 0,
            // debug: true,
        }).then(gatekeeper => import(moduleName).then(m => t.freshen(t => {
            reenable();
            t.assert(shutdownRef, true);
            t.assert(gatekeeper, fromResolve(Resolve({
                sturdyref: asSturdyRef(cap),
                observer: t.ref(during(async (t, ds) => {
                    const facet = t.facet(t => m.default(t, ds));
                    return t => t.stop(facet);
                })),
            })));
        })));
    });
});
