import { Actor, Assertion, Turn } from '../runtime/actor.js';
import { Relay, spawnRelay } from '../transport/relay.js';
import { Worker } from 'worker_threads';
import { fromInstance, Instance } from '../gen/worker.js';
import path from 'path';

export function spawnWorker(t: Turn, moduleName: string, arg: Assertion) {
    const w = new Worker(path.join(__dirname, 'wload.js'));
    const reenable = t.activeFacet.preventInertCheck();
    spawnRelay(t, {
        packetWriter: bs => w.postMessage(bs),
        setup(t: Turn, r: Relay) {
            w.on('message', bs => r.accept(bs));
            w.on('error', err => Turn.for(t.activeFacet, t => t.crash(err)));
            w.on('exit', code => Turn.for(t.activeFacet, t => {
                if (code === 0) {
                    t.stopActor();
                } else {
                    t.crash(new Error(`Worker crashed with code ${code}`));
                }
            }));
        },
        initialOid: 0,
    }).then(factory => {
        reenable();
        new Actor(t => t.assert(factory, fromInstance(Instance({
            name: moduleName,
            argument: arg,
        }))));
    });
}
