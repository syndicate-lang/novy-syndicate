import { Turn, Entity, Facet } from '../runtime/actor.js';
import readline from 'readline';

export function attachReadline(t: Turn, entity: Partial<Entity>): Facet {
    const ref = t.ref(entity);
    return t.facet(t => {
        let rl: readline.Interface | null =
            readline.createInterface({ input: process.stdin, output: process.stdout });
        function shutdown(_t: Turn) {
            rl?.close();
            rl = null;
        }
        t.assert(ref, true);
        t.activeFacet.actor.atExit(shutdown);
        t.activeFacet.onStop(shutdown);
        rl.on('line', (line: string) => t.freshen(t => t.message(ref, line)));
        rl.on('close', () => t.freshen(t => t.stop()));
    });
}
