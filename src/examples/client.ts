import { Embedded } from "@preserves/core";
import { $BoxState, fromSetBox, SetBox } from "../gen/examples/boxProtocol.js";
import { fromObserve, Observe, P } from "../runtime/dataspace.js";
import { Assertion, Ref, Turn } from "../runtime/actor.js";

export default function (t: Turn, ds_p: Embedded<Ref>) {
    const ds = ds_p.embeddedValue;
    console.log('Spawning Client');
    let count = 0;
    t.assert(ds, fromObserve(Observe({
        pattern: P.rec($BoxState, P.bind()),
        observer: t.ref({
            assert(t: Turn, [currentValue]: [number]): void {
                // console.log(`Client ${t.actor.id}: got ${currentValue}`);
                t.message(ds, fromSetBox(SetBox(currentValue + 1)));
            }
        })
    })));
    t.assert(ds, fromObserve(Observe({
        pattern: P.rec($BoxState, P.discard()),
        observer: t.ref({
            assert(_t: Turn, _assertion: Assertion): void {
                count++;
                // console.log('inc to', count, _assertion);
            },
            retract(t: Turn) {
                if (--count === 0) {
                    console.log(`Client ${t.activeFacet.id}: detected box termination`);
                    t.stopActor();
                }
                // console.log('dec to', count);
            },
        })
    })));
}
