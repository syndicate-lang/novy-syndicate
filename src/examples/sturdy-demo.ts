import { newKey } from '../transport/cryptography.js';
import { attenuate, KEY_LENGTH, mint, Caveat, Rewrite, sturdyEncode, validate } from '../transport/sturdy.js';
import * as RW from '../runtime/rewrite.js';
import { Bytes, Dictionary } from '@preserves/core';

async function main() {
    const m1 = await mint('hello world', new Bytes(KEY_LENGTH));
    console.log(m1);
    const m2 = await attenuate(m1, Caveat.Rewrite(Rewrite({
        pattern: RW.Pattern.PBind(RW.PBind(RW.Pattern.PCompound(RW.PCompound({
            ctor: RW.ConstructorSpec.CRec(RW.CRec({
                label: Symbol.for('says'),
                arity: 2
            })),
            members: new Dictionary<RW._embedded, RW.Pattern>([
                [0, RW.Pattern.Lit(RW.Lit('Tony'))]])
        })))),
        template: RW.Template.TRef(RW.TRef(0))
    })));
    console.log(m2);
    console.log('should be true:', await validate(m1, new Bytes(KEY_LENGTH)));
    console.log('should be true:', await validate(m2, new Bytes(KEY_LENGTH)));
    console.log('should be false:', await validate(m2, Bytes.of(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)));
    m2.oid = 'hello world2';
    console.log(m2);
    console.log('should be false:', await validate(m2, new Bytes(KEY_LENGTH)));
    m2.oid = 'hello world';
    console.log(m2);
    console.log('should be true:', await validate(m2, new Bytes(KEY_LENGTH)));
    console.log('should be false:', await validate(m2, await newKey()));
    console.log((await newKey()).asPreservesText());
    console.log((await newKey()).asPreservesText());
    console.log(sturdyEncode(RW.fromSturdyRef(m2)).asPreservesText());
}

main();
