import { Embedded } from "@preserves/core";
import { BoxState, $SetBox, fromBoxState } from "../gen/examples/boxProtocol.js";
import { fromObserve, Observe, P } from "../runtime/dataspace.js";
import { Assertion, Handle, Ref, Turn } from "../runtime/actor.js";

let startTime = Date.now();
let prevValue = 0;

export default function (t: Turn, arg: Assertion) {
    const [ds_p, LIMIT, REPORT_EVERY]: [Embedded<Ref>, number, number] = Array.isArray(arg) && arg.length === 3
        ? arg as any
        : [arg, 50000, 2500];
    const ds = ds_p.embeddedValue;
    console.log('Spawning Box', LIMIT, REPORT_EVERY);
    let valueHandle: Handle | undefined;
    function setValue(t: Turn, value: number) {
        valueHandle = t.replace(ds, valueHandle, fromBoxState(BoxState(value)));
    }
    setValue(t, 0);
    t.assert(ds, fromObserve(Observe({
        pattern: P.rec($SetBox, P.bind()),
        observer: t.ref({
            message(t: Turn, [newValue]: [number]): void {
                // console.log(`Box ${t.actor.id}: got ${newValue}`);
                if (newValue % REPORT_EVERY === 0) {
                    const endTime = Date.now();
                    const delta = (endTime - startTime) / 1000.0;
                    const count = newValue - prevValue;
                    prevValue = newValue;
                    startTime = endTime;
                    console.log(`Box ${t.activeFacet.id}: got ${newValue} (${count / delta} Hz)`);
                }
                if (newValue === LIMIT) t.stopActor();
                setValue(t, newValue);
            }
        })
    })));
}
