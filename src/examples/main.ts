import { Actor, Assertion, Turn } from '../runtime/actor.js';
import { Dictionary, embed } from '@preserves/core';
import { Dataspace } from '../runtime/dataspace.js';
import { _embedded, attenuate, CRec, Lit, Pattern, PCompound, rfilter, ConstructorSpec } from '../runtime/rewrite.js';
import { $BoxState, $SetBox } from '../gen/examples/boxProtocol.js';
import { $Observe } from '../gen/dataspace.js';
import { spawnWorker } from '../worker/index.js';
import path from 'path';

function spawnModule(t: Turn, moduleName: string, arg: Assertion) {
    import(moduleName).then(m => t.freshen(t => t.spawn(t => m.default(t, arg))));
}

// __setNextActorId(1000);

new Actor(async (t: Turn) => {
    const ds_unproxied = t.ref(new Dataspace());

    const ds = ds_unproxied;
    // const { proxy: ds, revoker } = forwarder(t, ds_unproxied);
    // t.spawn(t => {
    //     t.assert(ds, Observe(SetBox.constructorInfo.label, t.ref({
    //         message(t: Turn, [newValue]: [number]): void {
    //             if (newValue === 30000) {
    //                 console.log('BOOM!');
    //                 t.message(revoker, true);
    //             }
    //         }
    //     })));
    // });

    const ds_for_box = attenuate(
        ds,
        rfilter(
            Pattern.PCompound(PCompound({
                ctor: ConstructorSpec.CRec(CRec({ label: $BoxState, arity: 1 })),
                members: new Dictionary()
            })),
            Pattern.PCompound(PCompound({
                ctor: ConstructorSpec.CRec(CRec({ label: $Observe, arity: 2 })),
                members: new Dictionary<_embedded, Pattern>([
                    [0, Pattern.Lit(Lit($SetBox))]])
            }))));

    const ds_for_client = attenuate(
        ds_unproxied,
        rfilter(
            Pattern.PCompound(PCompound({
                ctor: ConstructorSpec.CRec(CRec({ label: $SetBox, arity: 1 })),
                members: new Dictionary()
            })),
            Pattern.PCompound(PCompound({
                ctor: ConstructorSpec.CRec(CRec({ label: $Observe, arity: 2 })),
                members: new Dictionary<_embedded, Pattern>([
                    [0, Pattern.Lit(Lit($BoxState))]])
            }))));

    const boxpath = path.join(__dirname, 'box.js');
    const clientpath = path.join(__dirname, 'client.js');

    t.activeFacet.preventInertCheck();

    // spawnModule(t, boxpath, [embed(ds_for_box), 500000, 25000]);
    spawnWorker(t, boxpath, [embed(ds_for_box), 50000, 2500]);

    spawnModule(t, clientpath, embed(ds_for_client));
    // spawnWorker(t, clientpath, embed(ds_for_client));
});
