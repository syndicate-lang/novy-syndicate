export default {
  input: 'lib/examples/sturdy-demo.js',
  output: {
    file: 'index.js',
    format: 'umd',
    name: 'Main',
    globals: {
      '@preserves/core': 'Preserves',
      'crypto': 'crypto', // this is a lie, but it lets the detection code in cryptography.ts run
    },
  },
  external: ['@preserves/core', 'crypto'],
};
