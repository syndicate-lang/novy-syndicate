import { message, Methods, Messages, perform } from './interfaces.js';

const m = message({
    selector: 'b',
    args: [123, 'hi', true],
    callback: (result: number) => console.log('result:', result),
});
const m2 = message({
    selector: 'c',
    args: [],
    // callback: (result: void) => console.log('result:', result),
});
type X1 = Methods<typeof m, [string]>;
type X2 = Methods<typeof m | typeof m2, [string]>;
type Y = Messages<X1, [string]>;

perform({
    a(ctxt: string, x: number): string { return `${ctxt}(${x + 1})`; },
    b(_ctxt: string, y: number): number { return y * 2; },
    z(_ctxt: string, ..._v: number[]) { return 3; },
    v(ctxt: string, x: number, y: string, z: boolean): string {
        console.log('in v');
        return `ctxt ${ctxt} x ${x} y ${y} z ${z}`;
    },
    w(ctxt: string, m: [string, number]) { console.log('w', ctxt, m); },
    w2(ctxt: string, x: number, ...m: [string, number][]) { console.log('w2', ctxt, x, m); },
    q(ctxt: string, n: number, m?: string): string {
        console.log('in q');
        return `q ctxt ${ctxt} n ${n} m ${m}`;
    },
},

// {
//     selector: 'w2',
//     args: [99, tuple('hi', 123), tuple('hi', 123)],
//     callback: (result: void) => console.log('result:', result)
// },

{
    selector: 'q',
    args: [123, 'hi', true],
    callback: (result: string) => console.log('result:', result)
},

// {
//     selector: 'v',
//     args: [123, 'hi', true],
//     callback: (result: string) => console.log('result:', result)
// },

'C');
